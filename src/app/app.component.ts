import { Component } from '@angular/core'
import { NgxPermissionsService } from 'ngx-permissions'
import { UserSessionHelper } from './helpers/user-session.helper'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'admin'

  constructor(
    private userSessionHelper: UserSessionHelper,
    private permissionsService: NgxPermissionsService) {

  }

  ngOnInit() {
    let user = this.userSessionHelper.getUser()
    const perm = [user.type_user]
    this.permissionsService.loadPermissions(perm)
  }
}
