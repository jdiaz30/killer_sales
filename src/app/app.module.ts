import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { XHRBackend, RequestOptions, HttpModule } from '@angular/http'
import { FormsModule } from '@angular/forms'
import { NgxPermissionsModule } from 'ngx-permissions'

import { AppComponent } from './app.component'
import { ThemeModule } from './theme/theme.module'
import { AppRoutingModule } from './app-routing.module'
import { HttpHelper } from './helpers/http.helper'
import { HttpService } from './services/http.service'
import { HttpFactory } from './app.settings'

import { AuthService } from './services/auth.service'
import { UserSessionHelper } from './helpers/user-session.helper'
import { AuthGuardService } from './services/auth-guard.service'
import { OperatorService } from './services/operator.service'
import { PromotorService } from './services/promotor.service'
import { CatalogService } from './services/catalog.service'
import { ActivityService } from './services/activity.service'
import { AvatarService } from './services/avatar.service'
import { ClanService } from './services/clan.service'
import { CitieService } from './services/citie.service'
import { CountryService } from './services/country.service'
import { HotelService } from './services/hotel.service'
import { OrderService } from './services/order-service.service'
import { PackageService } from './services/package.service'
import { CategoryService } from './services/category.service'
import { PickupService } from './services/pickup.service'
import { KcoinsService } from './services/kcoins.service'

import { GlobalHelper } from './helpers/global.helper'
import { SocketHelper } from './helpers/socket.helper'
import { NotificationHelper } from './helpers/notification.helper'

import { UserService } from './services/user.service'
import { DashboardService } from './services/dashboard.service'
import { NotFoundComponent } from './modules/not-found/not-found.component'
import { ClientService } from './services/client.service'
import { MoneyService } from './services/money.service'
import { DivisaService } from './services/divisa.service'

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    ThemeModule.forRoot(),
    NgxPermissionsModule.forRoot({
      permissionsIsolate: false,
      rolesIsolate: false
    })
    //
  ],
  providers: [
    HttpHelper,
    UserSessionHelper,
    SocketHelper,
    GlobalHelper,
    NotificationHelper,
    AuthService,
    CitieService,
    CountryService,
    HotelService,
    OperatorService,
    PromotorService,
    CatalogService,
    ActivityService,
    AvatarService,
    ClanService,
    OrderService,
    PackageService,
    CategoryService,
    PickupService,
    KcoinsService,
    UserService,
    ClientService,
    AuthGuardService,
    DashboardService,
    MoneyService,
    DivisaService,
    {
      provide: HttpService,
      useFactory: HttpFactory,
      deps: [XHRBackend, RequestOptions]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
