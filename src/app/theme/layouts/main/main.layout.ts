import { Component } from '@angular/core'

@Component({
    selector: 'theme-layout-main',
    templateUrl: './main.layout.html',
    styleUrls: ['./main.layout.scss']
})

export class MainLayoutComponent {
    title = 'admin'

    constructor() {

    }

    ngOnInit(): void {
    }
}
