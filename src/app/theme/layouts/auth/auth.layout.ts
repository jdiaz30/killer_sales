import { Component } from '@angular/core';

@Component({
    selector: 'theme-layout-auth',
    templateUrl: './auth.layout.html',
    styleUrls: ['./auth.layout.scss']
})
export class AuthLayoutComponent {
    title = 'admin';
}
