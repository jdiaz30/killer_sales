import { Directive, ElementRef, Input, HostListener, Renderer, SimpleChange } from '@angular/core';
import { element } from 'protractor';
import { HttpClientModule } from '@angular/common/http';
import { forEach } from '../../../../node_modules/@angular/router/src/utils/collection';
import { Validators } from '@angular/forms';


declare var moment: any
declare var $: any
declare var Unison: any
declare var window: any
declare var document: any
declare var jQuery: any

@Directive({
    selector: '[pickatime]'
})
export class PickatimeDirective {

    @Input('pickatime') pickatime: string
    @Input('ngValue') ngValue: string
    @Input('range') range: any = 30

    dateMoment: any
    element: any
    times: Array<any> = []


    constructor(private el: ElementRef, private renderer: Renderer) {
        //$(this.el.nativeElement).pickatime();
        //var today = new Date();
        this.inicialize()
    }

    ngOnChanges(changes: { [propName: string]: SimpleChange }) {
        if (changes['pickatime'] !== undefined) {
            this.pickatime = changes['pickatime'].currentValue;
            this.inicialize()
            this.changeOptionSelect()
        }
    }

    setPickatime() {
        this.pickatime = this.dateMoment.format('HH:mm:ss')
    }

    inicialize() {
        this.defineTimes()
        this.pickatime = (this.pickatime) ? this.pickatime : moment().format('HH:mm:ss')

        var dateMoment = moment(this.pickatime, 'HH:mm:ss')
        //console.log("INRANGE", dateMoment.isValid());
        if (dateMoment.isValid()) {
            this.clearOptions()
            var dim = this.times.length
            var times = [...this.times]
            this.times.forEach((time, index) => {
                this.addOptions(time.value, time.label, false)

                var datetimeStrigIni = moment(time.value, 'HH:mm:ss').unix()
                var optionA = false
                var optionB = true

                optionA = (dateMoment.unix() >= datetimeStrigIni)

                if (index < dim - 1) {
                    var datetimeStrigEnd = moment(times[index + 1].value, 'HH:mm:ss').unix()
                    optionB = (dateMoment.unix() < datetimeStrigEnd)
                }

                if (optionA && optionB) {
                    dateMoment = moment(time.value, 'HH:mm:ss')
                }
            })
            this.dateMoment = dateMoment

            this.setPickatime()
            this.changeOptionSelect()
        }
    }

    defineTimes() {
        var dateMoment = moment('0000-00-00 00:00:00', 'YYYY-mm-dd HH:mm:ss')
        this.element = $(this.el.nativeElement).html('')
        for (var i = 0; i < 24 * 60; i = i + this.range) {
            let time = dateMoment.format('HH:mm:ss')
            let timeMeridiam = dateMoment.format('hh:mm A')
            this.times.push({
                value: time,
                label: timeMeridiam
            })
            dateMoment.add(this.range, 'minutes')
        }
    }

    clearOptions() {
        this.element.html('')
    }

    addOptions(time, timeMeridiam, selected) {
        var textselect = (selected) ? 'selected="selected"' : ''
        this.element.append('<option value="' + time + '" ' + textselect + ' >' + timeMeridiam + '</option>')
    }

    changeOptionSelect() {

        this.clearOptions()
        this.times.forEach((time, index) => {
            if (this.pickatime === time.value) {
                this.addOptions(time.value, time.label, true)
            } else {
                this.addOptions(time.value, time.label, false)
            }
        })
    }


    /*
        * @param {object} The input object the timepicker is attached to.
        * @param {object} The object containing options
        */

    /*
        @HostListener('mouseover') onMouseover(btn) {
            console.log(this.el.nativeElement.value);
            this.renderer.setElementStyle(this.el.nativeElement, 'value', this.el.nativeElement.value);
            $(this.el.nativeElement).pickatime({ now: this.el.nativeElement.value });
        }
    */

}