import { Directive, ElementRef, SimpleChange, Input } from '@angular/core'
import { UserSessionHelper } from '../../helpers/user-session.helper';
import { GlobalVars } from '../../app.settings'
declare var $: any

@Directive({
    selector: '[search-country]'
})
export class SearchCountryDirective {

    @Input('placeholder') placeholder: string

    constructor(private el: ElementRef, private userSessionHelper: UserSessionHelper) {
        //el.nativeElement.style.backgroundColor = 'yellow';
        let token = this.userSessionHelper.getToken();
        let self = this
        self.placeholder = 'Buscar País'
        $(this.el.nativeElement).select2({
            placeholder: self.placeholder,
            minimumInputLength: 2,
            ajax: {
                url: GlobalVars.API_URL + '/countries/',
                headers: {
                    "Authorization": "JWT " + token,
                    "Content-Type": "application/json",
                },
                dataType: 'json',
                data: function (params) {
                    var query = {
                        name: params.term,
                        page: params.page || 1,
                        page_sizes: 10,
                        token: token
                    };
                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },

                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    var data = data.data.results;
                    var results = data.map(v => {
                        return {
                            id: v.id,
                            text: v.name
                        }
                    })
                    return {
                        results: results,
                    };
                }
            },
            /*escapeMarkup: function (markup) {
                console.log("escapeMarkup: ", markup);
                return markup;
            },
            formatRepo: function (repo) {
                console.log("formatRepo: ", repo);
                if (repo.loading) {
                    return repo.text;
                }
            },
            width: 'resolve'*/
        });
    }

    /*
    ngOnChanges(changes: { [propName: string]: SimpleChange }) {

        console.log("SearchCountryDirective")
    }
    */

}