import { Directive, ElementRef, SimpleChange, Input, Output, EventEmitter } from '@angular/core'
import { UserSessionHelper } from '../../helpers/user-session.helper';
import { GlobalVars } from '../../app.settings'
declare var $: any

@Directive({
    selector: '[select2]'
})
export class Select2Directive {

    @Input('placeholder') placeholder: string
    @Input('minimumInputLength') minimumInputLength: string
    @Input('ngModel') ngModel: string
    @Output() onChangeSelect2 = new EventEmitter();

    constructor(private el: ElementRef, private userSessionHelper: UserSessionHelper) {
        //el.nativeElement.style.backgroundColor = 'yellow';
        let token = this.userSessionHelper.getToken();
        let self = this
        let $el = $(this.el.nativeElement)

        $el.select2({
            //data: self.data,
            placeholder: self.placeholder,
            //minimumInputLength: self.minimumInputLength
        }).on('select2:select', function(e){
            var id = e.params.data.id
            $el.val(id).trigger("change")
            self.onChangeSelect2.emit(id)
        })
    }


    ngOnChanges(changes: { [propName: string]: SimpleChange }) {

        //console.log("CHANGE", changes);
        if (changes['ngModel'] !== undefined) {
            this.ngModel = changes['ngModel'].currentValue
            $(this.el.nativeElement).val(this.ngModel).trigger("change")
        }
        
    }

}