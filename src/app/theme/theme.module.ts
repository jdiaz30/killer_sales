// imports
import { BrowserModule } from '@angular/platform-browser'
import { NgModule, ModuleWithProviders } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { NgxPermissionsModule } from 'ngx-permissions'

import { AuthLayoutComponent } from './layouts/auth/auth.layout'
import { RouterModule } from '@angular/router'
import { MainLayoutComponent } from './layouts/main/main.layout'
import { HeaderComponent } from './components/header/header.component'
import { FooterComponent } from './components/footer/footer.component'
import { SideMenuComponent } from './components/sidemenu/sidemenu.component'
import { PaginationComponent } from './components/pagination/pagination.component'
import { PickatimeComponent } from './components/pickatime/pickatime.component'
import { MenuDirective } from './directives/menu.directive'
import { PickatimeDirective } from './directives/pickatime.directive'
import { Select2Directive } from './directives/select2.directive'

import { CommonModule } from '@angular/common'
// @NgModule decorator with its metadata

@NgModule({
    declarations: [
        AuthLayoutComponent,
        MainLayoutComponent,
        HeaderComponent,
        FooterComponent,
        SideMenuComponent,
        PaginationComponent,
        PickatimeComponent,
        MenuDirective,
        PickatimeDirective,
        Select2Directive
    ],
    imports: [
        RouterModule,
        CommonModule,
        NgxPermissionsModule.forChild()
    ],
    providers: [],
    exports: [
        AuthLayoutComponent,
        MainLayoutComponent,
        PaginationComponent,
        PickatimeComponent,
        PickatimeDirective,
        Select2Directive
    ]
})
export class ThemeModule {
    static forRoot(): ModuleWithProviders {
        return <ModuleWithProviders>{
            ngModule: ThemeModule,
            providers: [],
        };
    }
}