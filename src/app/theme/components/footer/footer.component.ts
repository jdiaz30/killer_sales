import { Component } from '@angular/core';

@Component({
    selector: 'theme-footer-cpn',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
    title = 'Footer';
}
