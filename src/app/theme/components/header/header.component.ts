import { Component } from '@angular/core'
import { Observable, Subject, Subscription } from 'rxjs'
import * as moment from 'moment'

import { SocketHelper } from '../../../helpers/socket.helper'
import { NotificationHelper } from '../../../helpers/notification.helper'
import { UserSessionHelper } from '../../../helpers/user-session.helper'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'theme-header-cpn',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

    private socket: any
    private messagesProfileSocket: Subscription
    private notifications: Array<any> = Array<any>()
    private userSession: any = null

    constructor(private socketHelper: SocketHelper,
        private notificationHelper: NotificationHelper,
        private userSessionHelper: UserSessionHelper,
        private router: Router) {

    }

    ngOnInit() {
        this.initSocket()
        this.userSession = this.userSessionHelper.getUser()
    }

    private initEvents() {
        this.messagesProfileSocket = this.socketHelper.fromEvent<any>("killer_sales:new-reservation")
            .subscribe((data) => {
                this.notificationHelper.showPrevious("Nueva reserva", "Hay una nueva reserva :" + data.name_client, "resevation")
                this.addNotification(data)

            }, (error) => {
                console.warn("initEvents error", error)
            })
    }

    private initSocket() {
        this.socket = this.socketHelper.getSocketInstance()
        if (this.socket == undefined || this.socket == null) {
            this.socketHelper.init()
            this.socket = this.socketHelper.getSocketInstance()
            this.initEvents()
        } else {
            this.initEvents()
        }
    }

    private addNotification(data) {
        this.notifications.push({
            id: data.id,
            title: data.name_client + " hizo una reserva",
            message: "Revisa la reserva aquí",
            date: moment(data.date_order).format("YYYY-MM-DD HH:mm:ss")
        })
    }

    private logout() {
        this.userSessionHelper.clearSession()
        this.router.navigate(['/'])
    }

}
