import { XHRBackend, RequestOptions } from '@angular/http';

import { HttpService } from './services/http.service';

export const GlobalVars = Object.freeze({

    API_URL: 'https://api.behlaaktours.xyz/api',
    API_BASE_URL: 'https://api.behlaaktours.xyz',
    API_NODE_URL: 'https://behlaaktours.xyz:3000'


    /*   API_URL: 'http://192.168.91.130:8000/api',
      API_BASE_URL: 'http://192.168.91.130:8000',
      API_NODE_URL: 'http://192.168.91.130:3000' */
})

export function HttpFactory(backend: XHRBackend, options: RequestOptions) {
    return new HttpService(backend, options)
}