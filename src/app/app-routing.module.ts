import { ExtraOptions, RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { NgxPermissionsGuard } from 'ngx-permissions'

import { AuthModule } from './modules/auth/auth.module'
import { ConfigModule } from './modules/config/config.module'
import { DashboardModule } from './modules/dashboard/dashboard.module'
import { AuthGuardService } from './services/auth-guard.service'
import { GameModule } from './modules/game/game.module'
import { DealsModule } from './modules/deals/deals.module'
import { NotFoundComponent } from './modules/not-found/not-found.component'
import { UserModule } from './modules/user/user.module'
import { PickupModule } from './modules/pickup/pickup.module'
import { CrmModule } from './modules/crm/crm.module'
import { ReportModule } from './modules/report/report.module'

const routes: Routes = [

    { path: '', redirectTo: 'auth', pathMatch: 'full' },
    { path: 'auth', loadChildren: () => AuthModule },
    { path: 'user', loadChildren: () => UserModule, canActivate: [AuthGuardService] },
    {
        path: 'crm', loadChildren: () => CrmModule, canActivate: [AuthGuardService, NgxPermissionsGuard], data: {
            permissions: {
                only: ['admin']
            }
        }
    },
    {
        path: 'config', loadChildren: () => ConfigModule, canActivate: [AuthGuardService, NgxPermissionsGuard], data: {
            permissions: {
                only: ['admin', 'operator']
            }
        }
    },
    {
        path: 'game', loadChildren: () => GameModule, canActivate: [AuthGuardService, NgxPermissionsGuard], data: {
            permissions: {
                only: 'admin'
            }
        }
    },
    {
        path: 'deals', loadChildren: () => DealsModule, canActivate: [AuthGuardService, NgxPermissionsGuard], data: {
            permissions: {
                only: ['admin', 'operator']
            }
        }
    },
    {
        path: 'pickup', loadChildren: () => PickupModule, canActivate: [AuthGuardService, NgxPermissionsGuard], data: {
            permissions: {
                only: ['admin', 'operator']
            }
        }
    },
    {
        path: 'report', loadChildren: () => ReportModule, canActivate: [AuthGuardService, NgxPermissionsGuard], data: {
            permissions: {
                only: ['admin', 'operator']
            }
        }
    },
    { path: 'dashboard', loadChildren: () => DashboardModule, canActivate: [AuthGuardService] },
    { path: '**', component: NotFoundComponent }
];

const config: ExtraOptions = {
    useHash: true,
};

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
