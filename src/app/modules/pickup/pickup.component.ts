import { Component } from '@angular/core';

declare var $: any
@Component({
    selector: 'module-pickup',
    //template: '<router-outlet></router-outlet>',
    template: '<theme-layout-main></theme-layout-main>'
})
export class PickupComponent {

    constructor() {

    }

    ngOnInit() {
        $('body').addClass("vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar")
    }
}
