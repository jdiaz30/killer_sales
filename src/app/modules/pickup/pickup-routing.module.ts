import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PickupComponent } from './pickup.component';
import { PickupActivityConfigPage } from './pages/pickup/activity/pickup-activity.page';
import { PickupConfigPage } from './pages/pickup/config/pickup-config.page';


const routes: Routes = [{
    path: '',
    component: PickupComponent,
    children: [
        {
            path: 'activity',
            component: PickupActivityConfigPage
        },
        {
            path: 'activity/:id/config',
            component: PickupConfigPage
        },
    ],

},

    /* { path: '/config', redirectTo: '/config/operator' }, */
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class PickupRoutingModule {
}
