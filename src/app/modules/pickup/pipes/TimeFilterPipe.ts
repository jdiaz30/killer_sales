import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'timeFilterPipe' })
export class TimeFilterPipe implements PipeTransform {
    transform(data: Array<any>, timeFilter: string): Array<any> {
        console.log("timeFilter", timeFilter)
        if (timeFilter == "0") {
            return data
        } else {
            let timeList = (timeFilter == "am") ? data.filter((time, index) => index < 144) : data.filter((time, index) => index >= 144)
            return timeList
        }

    }
}