import { NgModule } from '@angular/core'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http'

import { ThemeModule } from '../../theme/theme.module'

import { CommonModule } from '../../../../node_modules/@angular/common'
import { PickupComponent } from './pickup.component'
import { PickupActivityConfigPage } from './pages/pickup/activity/pickup-activity.page'
import { PickupConfigPage } from './pages/pickup/config/pickup-config.page'
import { PickupRoutingModule } from './pickup-routing.module'
import { TimeFilterPipe } from './pipes/TimeFilterPipe'


const PAGES_COMPONENTS = [
    PickupComponent,
    PickupActivityConfigPage,
    PickupConfigPage
]

const PIPES = [
    TimeFilterPipe
]


@NgModule({
    imports: [
        CommonModule,
        PickupRoutingModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
    ],
    declarations: [
        ...PAGES_COMPONENTS,
        ...PIPES
    ],
    exports: [
        ...PIPES
    ],
    providers: [
    ],

})
export class PickupModule {
}
