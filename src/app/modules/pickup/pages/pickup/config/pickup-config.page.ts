import { Component } from '@angular/core'
import { Observable, Subject, Subscription } from 'rxjs'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'

import { Router, ActivatedRoute } from '@angular/router'
import * as moment from 'moment';
import { CatalogService } from '../../../../../services/catalog.service';
import { OperatorService } from '../../../../../services/operator.service';
import { UserSessionHelper } from '../../../../../helpers/user-session.helper';
import { PickupService } from '../../../../../services/pickup.service';
import { ActivityService } from '../../../../../services/activity.service';
import { map } from 'rxjs/operators';
import { GlobalHelper } from 'src/app/helpers/global.helper';

declare var swal: any
declare var $: any

@Component({
    selector: 'app-pickup-config-page',
    templateUrl: './pickup-config.page.html',
    styleUrls: ['./pickup-config.page.scss']
})

export class PickupConfigPage {

    private activityList: Array<any> = new Array<any>()
    private pickupList: Array<any> = new Array<any>()
    private times: Array<any> = []

    private frmPickup: FormGroup
    private frmPickupClone: FormGroup
    private pickupId: number = null
    private isEditable: boolean = false
    private activity: any = null
    // private isUpdate: boolean = false
    private count = 0
    private pageSize = 30
    private page = 1
    private activityId: any
    testSearch: string = ''
    catalogoId: string
    operatorId: string

    private days: Array<any> = new Array<any>()
    private isCheckedDays: boolean = false

    constructor(
        private catalogService: CatalogService,
        private operatorService: OperatorService,
        private userSessionHelper: UserSessionHelper,
        private globalHelper: GlobalHelper,
        private pickupService: PickupService,
        private activityService: ActivityService,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        let id = this.route.snapshot.paramMap.get('id')

        if (id !== null) {
            this.activityId = parseInt(id)
            this.getActivity()
            this.initTimes()
            this.listPickup()
            this.createForm()
            //this.isUpdate = true
        }

    }

    private onAddressChange(event) {
        let value = event.target.value
        this.frmPickup.controls['lu_address'].setValue(value)
        this.frmPickup.controls['ma_address'].setValue(value)
        this.frmPickup.controls['mi_address'].setValue(value)
        this.frmPickup.controls['ju_address'].setValue(value)
        this.frmPickup.controls['vi_address'].setValue(value)
        this.frmPickup.controls['sa_address'].setValue(value)
        this.frmPickup.controls['do_address'].setValue(value)
    }

    public applyAllCheckeds() {

        if (this.frmPickup.value.lu_checked) {
            this.frmPickup.controls['lu_address'].setValue(this.frmPickup.value.checked_address)
            this.frmPickup.controls['lu_time'].setValue(this.frmPickup.value.checked_time)
        }
        if (this.frmPickup.value.ma_checked) {
            this.frmPickup.controls['ma_address'].setValue(this.frmPickup.value.checked_address)
            this.frmPickup.controls['ma_time'].setValue(this.frmPickup.value.checked_time)
        }
        if (this.frmPickup.value.mi_checked) {
            this.frmPickup.controls['mi_address'].setValue(this.frmPickup.value.checked_address)
            this.frmPickup.controls['mi_time'].setValue(this.frmPickup.value.checked_time)
        }
        if (this.frmPickup.value.ju_checked) {
            this.frmPickup.controls['ju_address'].setValue(this.frmPickup.value.checked_address)
            this.frmPickup.controls['ju_time'].setValue(this.frmPickup.value.checked_time)
        }
        if (this.frmPickup.value.vi_checked) {
            this.frmPickup.controls['vi_address'].setValue(this.frmPickup.value.checked_address)
            this.frmPickup.controls['vi_time'].setValue(this.frmPickup.value.checked_time)
        }
        if (this.frmPickup.value.sa_checked) {
            this.frmPickup.controls['sa_address'].setValue(this.frmPickup.value.checked_address)
            this.frmPickup.controls['sa_time'].setValue(this.frmPickup.value.checked_time)
        }
        if (this.frmPickup.value.do_checked) {
            this.frmPickup.controls['do_address'].setValue(this.frmPickup.value.checked_address)
            this.frmPickup.controls['do_time'].setValue(this.frmPickup.value.checked_time)
        }

    }

    public changeFilter(event) {
        this.listPickup()
    }

    public changePage(page) {
        this.page = page
        this.listPickup()
    }

    initTimes() {
        var size = 5
        var dateMoment = moment('00:00:00', 'HH:mm:ss')
        for (var i = 0; i < 24 * 60; i = i + size) {
            let time = dateMoment.format('HH:mm:ss')
            let timeMeridiam = dateMoment.format('hh:mm A')
            this.times.push({
                value: time,
                label: timeMeridiam
            })
            dateMoment.add(size, 'minutes')
        }
    }

    private handleSelectedAll() {
        this.frmPickup.patchValue({
            lu_checked: this.frmPickup.value.checked_all,
            ma_checked: this.frmPickup.value.checked_all,
            mi_checked: this.frmPickup.value.checked_all,
            ju_checked: this.frmPickup.value.checked_all,
            vi_checked: this.frmPickup.value.checked_all,
            sa_checked: this.frmPickup.value.checked_all,
            do_checked: this.frmPickup.value.checked_all
        })
    }
    private handleSelectedTimeFilter() {
        console.log("handleSelectedTimeFilter", this.frmPickup.value.time_filter)
    }

    private createForm() {
        this.pickupId = 0

        this.frmPickupClone = this.frmBuilder.group({
            activityId: new FormControl('0')
        })

        this.frmPickup = this.frmBuilder.group({
            is_checked_all: new FormControl(false),
            checked_all: new FormControl(false),
            checked_address: new FormControl(''),
            time_filter: new FormControl('0'),
            checked_time: new FormControl(''),
            activity_id: new FormControl(''),
            lu_checked: new FormControl(false),
            lu_address: new FormControl(''),
            lu_time: new FormControl(''),
            ma_checked: new FormControl(false),
            ma_address: new FormControl(''),
            ma_time: new FormControl(''),
            mi_checked: new FormControl(false),
            mi_address: new FormControl(''),
            mi_time: new FormControl(''),
            ju_checked: new FormControl(false),
            ju_address: new FormControl(''),
            ju_time: new FormControl(''),
            vi_checked: new FormControl(false),
            vi_address: new FormControl(''),
            vi_time: new FormControl(''),
            sa_checked: new FormControl(false),
            sa_address: new FormControl(''),
            sa_time: new FormControl(''),
            do_checked: new FormControl(false),
            do_address: new FormControl(''),
            do_time: new FormControl('')
        })

    }

    private createPickup() {
        //this.activity = null
        this.isEditable = false
        //this.createForm()
        $("#pickupForm").modal('show')

    }

    private clonePickupAll() {
        $("#pickupCloneForm").modal('show')
    }

    private clonePickup(pickup) {
        this.isEditable = false
        this.setPickup(pickup)
        $("#pickupForm").modal('show')
    }

    private editPickup(pickup: any) {
        this.isEditable = true
        this.setPickup(pickup)
        $("#pickupForm").modal('show')
    }

    private clearTime(indexDay: number) {
        this.frmPickup.patchValue({ checked_all: false })
        switch (indexDay) {
            case 0:
                this.frmPickup.patchValue({ lu_time: "", lu_checked: false })
                break
            case 1:
                this.frmPickup.patchValue({ ma_time: "", ma_checked: false })
                break
            case 2:
                this.frmPickup.patchValue({ mi_time: "", mi_checked: false })
                break
            case 3:
                this.frmPickup.patchValue({ ju_time: "", ju_checked: false })
                break
            case 4:
                this.frmPickup.patchValue({ vi_time: "", vi_checked: false })
                break
            case 5:
                this.frmPickup.patchValue({ sa_time: "", sa_checked: false })
                break
            case 6:
                this.frmPickup.patchValue({ do_time: "", do_checked: false })
                break
        }
    }

    private setPickup(pickup: any) {
        this.frmPickup.patchValue({
            activity_id: pickup.activity_id,
            lu_address: pickup.lu_address,
            lu_time: pickup.lu_time,
            ma_address: pickup.ma_address,
            ma_time: pickup.ma_time,
            mi_address: pickup.mi_address,
            mi_time: pickup.mi_time,
            ju_address: pickup.ju_address,
            ju_time: pickup.ju_time,
            vi_address: pickup.vi_address,
            vi_time: pickup.vi_time,
            sa_address: pickup.sa_address,
            sa_time: pickup.sa_time,
            do_address: pickup.do_address,
            do_time: pickup.do_time,
            checked_address: pickup.lu_address
        })
        this.pickupId = pickup.id
    }

    public deletePickup(pickup, index) {
        let self = this
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar pickup?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.pickupService.delete(pickup.id).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                        self.pickupList.splice(index, 1);
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private closePickup() {
        this.pickupId = 0
        this.frmPickup.reset()
    }

    private save() {
        if (this.frmPickup.valid == true) {
            let data = {
                activity_id: this.activity.id,
                lu_address: this.frmPickup.value.lu_address,
                lu_time: this.frmPickup.value.lu_time,
                ma_address: this.frmPickup.value.ma_address,
                ma_time: this.frmPickup.value.ma_time,
                mi_address: this.frmPickup.value.mi_address,
                mi_time: this.frmPickup.value.mi_time,
                ju_address: this.frmPickup.value.ju_address,
                ju_time: this.frmPickup.value.ju_time,
                vi_address: this.frmPickup.value.vi_address,
                vi_time: this.frmPickup.value.vi_time,
                sa_address: this.frmPickup.value.sa_address,
                sa_time: this.frmPickup.value.sa_time,
                do_address: this.frmPickup.value.do_address,
                do_time: this.frmPickup.value.do_time,
            }

            if (!this.isEditable) {
                this.create(data)
            } else {
                this.update(data)
            }
            this.closePickup()
            $("#pickupForm").modal('hide')
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.pickupService.save(data).subscribe(
            (response) => {
                this.pickupList.push(response.data)
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.pickupService.update(data, this.pickupId, {}).subscribe(
            (response) => {
                var pickup = response.data
                var index = this.pickupList.findIndex((item) => item.id === pickup.id)
                this.pickupList[index] = pickup
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private listPickup() {
        this.pickupService.getAll({ page_size: this.pageSize, page: this.page, name: this.testSearch, activity_id: this.activityId }).subscribe(
            (response) => {
                this.pickupList = response.data.results;
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private getActivity() {
        this.activity = JSON.parse(localStorage.getItem("pickup/activity"))
        this.listActivities(this.activity.catalog_id)
    }

    private clonePickupSave() {
        let data = { activity_id: this.frmPickupClone.value.activityId }
        $("#pickupCloneForm").modal('hide')
        this.globalHelper.showLoading()
        this.activityService.clonePickups(this.activity.id, data).subscribe(
            (response) => {
                this.globalHelper.hideShowLoading(window, 500)
                this.reload()
                swal("Info", response.user_msg, "success")
            },
            (error) => {
                this.globalHelper.hideShowLoading(window, 500)
                swal("Error", "No se pudo clonar los pickups", "warning")
            })
    }

    private listActivities(catalogId: number) {
        this.catalogService.getAllActivities(catalogId).subscribe(
            (response) => {
                this.activityList = response.data
            },
            (error) => {

            }
        )
    }

    private reload() {
        this.router.navigate(['/pickup/activity/' + this.activity.id + '/config'])
    }

}
