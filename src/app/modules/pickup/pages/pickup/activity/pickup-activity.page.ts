import { Component } from '@angular/core'
import { Observable, Subject, Subscription } from 'rxjs'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'

import { Router } from '@angular/router'
import * as moment from 'moment';
import { CatalogService } from '../../../../../services/catalog.service'
import { OperatorService } from '../../../../../services/operator.service'
import { UserSessionHelper } from '../../../../../helpers/user-session.helper'
import { PickupService } from '../../../../../services/pickup.service'
import { ActivityService } from '../../../../../services/activity.service'
import { map } from 'rxjs/operators';
import { GlobalVars } from 'src/app/app.settings'
import { GlobalHelper } from 'src/app/helpers/global.helper';

declare var swal: any
declare var $: any

@Component({
    selector: 'app-pickup-activity-page',
    templateUrl: './pickup-activity.page.html',
    styleUrls: ['./pickup-activity.page.scss']
})

export class PickupActivityConfigPage {

    private urlBaseImage = GlobalVars.API_BASE_URL

    private operatorsList: Array<any> = new Array<any>()
    private catalogoList: Array<any> = new Array<any>()
    private activityList: Array<any> = new Array<any>()
    private pickupList: Array<any> = new Array<any>()
    private times: Array<any> = []

    private frmPickup: FormGroup
    private pickupId: number = null
    private isEditable: boolean = false
    private activity: any = null

    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''
    private catalogId: number
    private operatorId: number

    constructor(
        private catalogService: CatalogService,
        private operatorService: OperatorService,
        private userSessionHelper: UserSessionHelper,
        private globalHelper: GlobalHelper,
        private pickupService: PickupService,
        private activityService: ActivityService,
        private frmBuilder: FormBuilder,
        private router: Router,
    ) {

    }

    ngOnInit() {
        this.getStorages()
        this.initByTypeUser()
    }


    public changeFilter(event) {

    }

    public changePage(page) {
        this.page = page
    }

    private initByTypeUser() {
        let user = this.userSessionHelper.getUser()
        this.listOperator()
    }

    private listOperator() {
        let params = { page_size: 100 }
        params = this.globalHelper.setOperatorParams(params)
        this.operatorService.getAll(params).subscribe(
            (response) => {
                this.operatorsList = response.data.results;
                this.catalogoList = []
                if (!this.operatorId) {
                    this.operatorId = (this.operatorsList.length > 0) ? this.operatorsList[0].id : null
                }
                this.listCatalogo()
            },
            (error) => {

            }
        )
    }

    private listCatalogo() {
        if (this.operatorId) {
            this.catalogService.getAll({ page_size: 100, operator_id: this.operatorId }).subscribe(
                (response) => {
                    this.catalogoList = response.data.results
                    this.catalogId = null
                    if (!this.catalogId) {
                        this.catalogId = (!this.catalogId && this.catalogoList.length > 0) ? this.catalogoList[0].id : null
                    }
                    this.activityList = []
                    this.listActivities()
                },
                (error) => {

                }
            )
        } else {
            this.catalogId = null
        }
    }

    private listActivities() {
        if (this.catalogId) {
            this.catalogService.getAllActivities(this.catalogId).subscribe(
                (response) => {
                    this.activityList = response.data
                },
                (error) => {

                }
            )
        }
    }

    private configPickup(activity) {
        localStorage.setItem("pickup/activity", JSON.stringify(activity))
        this.router.navigate(['/pickup/activity/' + activity.id + '/config'])
    }

    private getStorages() {
        var activity = JSON.parse(localStorage.getItem("pickup/activity"))
        if (activity) {
            this.catalogId = activity.catalog.id
            this.operatorId = activity.catalog.operator.id
        }
    }

}
