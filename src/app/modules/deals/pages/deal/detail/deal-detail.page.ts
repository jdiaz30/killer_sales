import { Component } from '@angular/core'
import { PromotorService } from '../../../../../services/promotor.service'
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment'

import { OrderService } from '../../../../../services/order-service.service'
import { GlobalHelper } from '../../../../../helpers/global.helper'
import { ActivityService } from 'src/app/services/activity.service';

declare var swal: any
declare var $: any

@Component({
    selector: 'app-deal-detail-page',
    templateUrl: './deal-detail.page.html',
    styleUrls: ['./deal-detail.page.scss']
})

export class DealDetailPage {
    private orderData = {
        order: null,
        activities: []
    }
    private totalData = { childs: 0, adults: 0, total: 0 }
    private activityPickUps: Array<any> = new Array<any>()
    private viewType = "report"
    private txtSearchPickup: string = ""

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private orderService: OrderService,
        private activityService: ActivityService,
        private globalHelper: GlobalHelper
    ) {

    }

    ngOnInit() {
        let id = this.route.snapshot.paramMap.get('id')
        let self = this
        if (id !== null) {
            this.getOrderData(id)
        }

        $("#pickupForm").on("hidden.bs.modal", function () {
            self.activityPickUps.length = 0
        })
    }

    private handleSelectView(option: string) {
        this.viewType = option
        this.parseOrderData()
    }

    private showPickUp(activity: any) {

        this.activityService.getPickups(activity.activity_id, { page_size: 100 }).subscribe(
            (response) => {
                response.data.results.map((pickup) => {
                    let parsePickUp = this.parsePickUp(activity.pick_up_date, pickup, activity.activity_id)
                    parsePickUp.active = (parsePickUp.id == activity.pickup.id) ? true : false
                    this.activityPickUps.push(parsePickUp)
                })

                this.txtSearchPickup = activity.pickup.lu_address.substring(0, 1)
            },
            (error) => {

            }
        )
    }

    private selectedPickUp(pickup: any) {

        let data = {
            activity_id: pickup.activityId,
            pickup_id: pickup.id
        }

        let params = {
            action: "pickup_update"
        }

        this.globalHelper.showLoading()
        this.orderService.updateOrderActivity(params, this.orderData.order.id, data).subscribe(
            (response) => {
                let orderActivity = response.data
                this.globalHelper.hideShowLoading(window, 500)
                this.orderData.activities.map((activity) => {
                    if (activity.activity_id == orderActivity.activity_id) {
                        activity.pickup = orderActivity.pickup
                        activity.pickup_text = this.getPickUpText(activity.pick_up_date, orderActivity.pickup, activity.id)
                    }
                    return activity
                })
            },
            (error) => {
                this.globalHelper.hideShowLoading(window, 500)
            })
        $("#pickupForm").modal("hide")
    }

    private updateStatus(status: number) {
        let data = {
            status: status,
            date_order_upd: moment().utc().format("YYYY-MM-DD HH:mm:ss")
        }
        this.globalHelper.showLoading()
        this.orderService.updateStatus(this.orderData.order.id, data, {}).subscribe(
            (response) => {
                this.globalHelper.hideShowLoading(window, 500)
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/deals/deal/'])
            },
            (error) => {
                this.globalHelper.hideShowLoading(window, 500)
                swal("Aviso", "No se pudo actualizar la reserva", "warning")
            }
        )
    }

    private getOrderData(orderId: any) {
        this.orderService.get(orderId).subscribe(
            (response) => {
                this.orderData = response.data
                this.parseOrderData()
            },
            (error) => {

            })
    }

    private parseOrderData() {
        this.totalData.childs = 0
        this.totalData.adults = 0

        this.orderData.activities.map((activity) => {
            let priceChild = (this.viewType == "report") ? activity.price_rep_child : activity.price_pub_child
            let priceAdult = (this.viewType == "report") ? activity.price_rep_adult : activity.price_pub_adult
            this.totalData.childs += (this.orderData.order.nro_childs > 0) ? parseFloat(priceChild) : 0
            this.totalData.adults += (this.orderData.order.nro_adults > 0) ? parseFloat(priceAdult) : 0
            activity.pickup_text = this.getPickUpText(activity.pick_up_date, activity.pickup, activity.id)
            return activity
        })

        this.totalData.childs = this.totalData.childs * ((this.orderData.order.nro_childs == 0) ? 1 : this.orderData.order.nro_childs)
        this.totalData.adults = this.totalData.adults * ((this.orderData.order.nro_adults == 0) ? 1 : this.orderData.order.nro_adults)

        this.orderData.order.total = this.totalData.childs + this.totalData.adults
    }

    private getPickUpText(date: string, pickup: any, activityId: number) {
        date = date.substr(0, 10)
        let parsePickUp = this.parsePickUp(date, pickup, activityId)
        let pickupText = parsePickUp.address + " - " + date + " " + parsePickUp.time
        return pickupText
    }

    private parsePickUp(datePickUp: string, pickup: any, activityId: number) {
        let pickUpParse = { address: pickup.lu_address, time: "", id: pickup.id, active: false, activityId: activityId }
        let date = moment(datePickUp).day()

        switch (date) {
            case 0:
                pickUpParse.time = pickup.do_time
                break
            case 1:
                pickUpParse.time = pickup.lu_time
                break
            case 2:
                pickUpParse.time = pickup.ma_time
                break
            case 3:
                pickUpParse.time = pickup.mi_time
                break
            case 4:
                pickUpParse.time = pickup.ju_time
                break
            case 5:
                pickUpParse.time = pickup.vi_time
                break
            case 6:
                pickUpParse.time = pickup.sa_time
                break
        }
        return pickUpParse
    }
}
