import { Component } from '@angular/core'
import { Observable, Subject, Subscription } from 'rxjs'

import { Router } from '@angular/router'
import * as moment from 'moment-timezone'

import { OrderService } from '../../../../../services/order-service.service'
import { GlobalHelper } from '../../../../../helpers/global.helper'
import { SocketHelper } from '../../../../../helpers/socket.helper'
import { PromotorService } from '../../../../../services/promotor.service'
import { UserSessionHelper } from 'src/app/helpers/user-session.helper';
import { GlobalVars } from 'src/app/app.settings'

declare var swal: any
declare var $: any

@Component({
    selector: 'app-deal-page',
    templateUrl: './deal-list.page.html',
    styleUrls: ['./deal-list.page.scss']
})

export class DealListPage {

    private orderList: Array<any> = new Array<any>()
    private dateIni = moment().format("YYYY-MM-DD")
    private dateEnd = moment().format("YYYY-MM-DD")
    private orderStats = { pendients: 0, reserveds: 0, canceleds: 0, factured: 0 }
    private ordersSocket: Subscription
    private socket: any

    public urlBaseImage = GlobalVars.API_BASE_URL

    constructor(
        private router: Router,
        private orderService: OrderService,
        private userSessionHelper: UserSessionHelper,
        private globalHelper: GlobalHelper,
        private socketHelper: SocketHelper,
    ) {

    }

    ngOnInit() {
        let self = this
        let userSession = this.userSessionHelper.getUser()

        this.socket = this.socketHelper.getSocketInstance()
        $('.daterange').daterangepicker({}, function (start, end, label) {
            /* self.dateIni = moment(start).utc().format("YYYY-MM-DD")
            self.dateEnd = moment(end).utc().format("YYYY-MM-DD") */

            self.dateIni = moment(start).format("YYYY-MM-DD")
            self.dateEnd = moment(end).add(1, 'days').format("YYYY-MM-DD")

            let params = { page_size: 100, date_order_ini: self.dateIni, date_order_end: self.dateEnd }
            params = self.globalHelper.setOperatorParams(params)

            self.listOrders(params)
            self.listOrdersStats({ date_order_ini: self.dateIni, date_order_end: self.dateEnd })
        })

        let params = { page_size: 100, date_order: this.dateIni }
        params = self.globalHelper.setOperatorParams(params)

        this.listOrders(params)
        this.listOrdersStats({ date_order: this.dateIni })
        this.initEvents()
    }

    private initEvents() {
        let self = this
        this.ordersSocket = this.socketHelper.fromEvent<any>("killer_sales:new-reservation")
            .subscribe((data) => {
                self.addOrders(data)
            }, (error) => {
                console.warn("initEvents error", error)
            })
    }

    private addOrders(data) {
        this.orderList.unshift(data)
        if (data.status == 0) {
            this.orderStats.pendients += 1
        } else if (data.status == 1) {
            this.orderStats.reserveds += 1
        } else if (data.status == 2) {
            this.orderStats.canceleds += 1
        }
    }

    private listOrders(params) {
        this.globalHelper.showLoading()
        this.orderService.getAll(params).subscribe(
            (response) => {
                this.orderList = response.data.results
                this.globalHelper.hideShowLoading(window, 500)

                setTimeout(() => {
                    $('[data-toggle="tooltip"]').tooltip({
                        container: 'body'
                    })
                }, 2000)
            },
            (error) => {
                this.globalHelper.hideShowLoading(window, 500)
            }
        )
    }

    private listOrdersStats(params) {
        this.orderService.getStats(params).subscribe(
            (response) => {
                this.orderStats.canceleds = response.data.total_canceleds
                this.orderStats.pendients = response.data.total_pendients
                this.orderStats.reserveds = response.data.total_reserveds
                this.orderStats.factured = response.data.total_factured

                console.log("listOrdersStats", this.orderStats)
            },
            (error) => {

            }
        )
    }

    private showDetail(orderId) {
        this.router.navigate(['/deals/deal/' + orderId + '/detail']);
    }

}
