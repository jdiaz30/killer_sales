import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'statusButtonColorPipe' })
export class StatusButtonColor implements PipeTransform {
    transform(value: number, exponent: string): string {
        let colorButton = ""
        switch (value) {
            case 0:
                colorButton = exponent + "info"
                break
            case 1:
                colorButton = exponent + "success"
                break
            case 2:
                colorButton = exponent + "warning"
                break
            case 3:
                colorButton = exponent + "danger"
                break
        }
        return colorButton
    }
}