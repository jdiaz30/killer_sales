import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'searchPickupPipe' })
export class SearchPickUpPipe implements PipeTransform {
    transform(list: any[], filterText: string): any {
        return list ? list.filter(item => item.address.search(new RegExp(filterText, 'i')) > -1) : []
    }
}