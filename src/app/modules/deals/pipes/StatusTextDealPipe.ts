import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'statusTextDealPipe' })
export class StatusTextDealPipe implements PipeTransform {
    transform(value: number, exponent: string): string {
        let response = ""
        switch (value) {
            case 0:
                response = "Pendiente"
                break
            case 1:
                response = "Reservado"
                break
            case 2:
                response = "Pagado"
                break
            case 3:
                response = "Cancelado"
                break
        }

        return response
    }
}