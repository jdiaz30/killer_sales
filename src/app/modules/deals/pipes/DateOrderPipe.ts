import { Pipe, PipeTransform } from '@angular/core'
import * as moment from 'moment-timezone'

@Pipe({ name: 'dateOrderPipe' })
export class DateOrderPipe implements PipeTransform {
    transform(value: string, format: string): string {
        let timezone = moment.tz.guess()
        let date = moment(value).tz(timezone).format(format)
        return date
    }
}