import { NgModule } from '@angular/core';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';


import { ThemeModule } from '../../theme/theme.module';

import { CommonModule } from '../../../../node_modules/@angular/common';
import { DealComponent } from './deals.component';
import { DealListPage } from './pages/deal/list/deal-list.page';
import { DealsRoutingModule } from './deals-routing.module';
import { StatusButtonColor } from './pipes/StatusButtonColorPipe';
import { StatusTextDealPipe } from './pipes/StatusTextDealPipe';
import { DateOrderPipe } from './pipes/DateOrderPipe';
import { DealDetailPage } from './pages/deal/detail/deal-detail.page';
import { SearchPickUpPipe } from './pipes/SearchPickUpPipe';


const PAGES_COMPONENTS = [
    DealComponent,
    DealListPage,
    DealDetailPage
]

const PIPES = [
    StatusButtonColor,
    StatusTextDealPipe,
    DateOrderPipe,
    SearchPickUpPipe
]

@NgModule({
    imports: [
        CommonModule,
        DealsRoutingModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
    ],
    declarations: [
        ...PAGES_COMPONENTS,
        ...PIPES
    ],
    providers: [
    ],
    exports: [
        ...PIPES
    ]
})
export class DealsModule {
}
