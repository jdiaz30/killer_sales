import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DealComponent } from './deals.component';
import { DealListPage } from './pages/deal/list/deal-list.page';
import { DealDetailPage } from './pages/deal/detail/deal-detail.page';


const routes: Routes = [{
    path: '',
    component: DealComponent,
    children: [

        {
            path: 'deal',
            component: DealListPage
        },
        {
            path: 'deal/:id/detail',
            component: DealDetailPage
        },

    ],

},

    /* { path: '/config', redirectTo: '/config/operator' }, */
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class DealsRoutingModule {
}
