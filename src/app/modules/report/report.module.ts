import { NgModule } from '@angular/core'

import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http'

import { ThemeModule } from '../../theme/theme.module'
import { CommonModule } from '../../../../node_modules/@angular/common'
import { ReportRoutingModule } from './report-routing.module'
import { ReportDealsListPage } from './pages/deals/list/report-deals-list'
import { ReportComponent } from './report.component'
import { ReportDealsKillerPage } from './pages/deals/killer/report-deals-killer'

import { DealsModule } from '../deals/deals.module'


const PAGES_COMPONENTS = [
    ReportDealsListPage,
    ReportDealsKillerPage,
    ReportComponent
]

@NgModule({
    imports: [
        CommonModule,
        ReportRoutingModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        DealsModule
    ],
    declarations: [
        ...PAGES_COMPONENTS,
    ],
    providers: [
    ]
})
export class ReportModule {
}
