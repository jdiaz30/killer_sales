import { Component } from '@angular/core'
import { GlobalVars } from '../../../../../app.settings'
import { Router } from '@angular/router'

declare var swal: any

@Component({
    selector: 'app-report-deals-list-page',
    templateUrl: './report-deals-list.html',
    styleUrls: ['./report-deals-list.scss']
})

export class ReportDealsListPage { }