import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators'
import * as moment from 'moment-timezone'


import { GlobalVars } from '../../../../../app.settings'
import { PromotorService } from 'src/app/services/promotor.service'
import { OrderService } from 'src/app/services/order-service.service'
import { HttpHelper } from 'src/app/helpers/http.helper'
import { UserSessionHelper } from 'src/app/helpers/user-session.helper'

declare var swal: any
declare var $: any

@Component({
    selector: 'app-report-deals-killer-page',
    templateUrl: './report-deals-killer.html',
    styleUrls: ['./report-deals-killer.scss']
})

export class ReportDealsKillerPage {

    private frmReport: FormGroup
    private searchField: FormControl
    private userSession: any = null
    private promotorList: Array<any> = new Array<any>()
    private dealList: Array<any> = new Array<any>()
    private count = 0
    private pageSize = 3
    private page = 1

    constructor(
        private router: Router,
        private frmBuilder: FormBuilder,
        private promotorService: PromotorService,
        private httpHelper: HttpHelper,
        private orderService: OrderService,
        private userSessionHelper: UserSessionHelper
    ) {

    }

    ngOnInit() {
        let self = this
        this.createForm()
        this.userSession = this.userSessionHelper.getUser()

        console.log("userSession", this.userSession)

        $('.daterange').daterangepicker({}, function (start, end, label) {
            let dateStart = moment(start).format("YYYY-MM-DD")
            let dateEnd = moment(end).format("YYYY-MM-DD")

            self.frmReport.patchValue({ dateStart: dateStart, dateEnd: dateEnd })
        })
    }

    private exportExcel() {
        let token = this.userSessionHelper.getToken()
        let { promotorId, status, dateStart, dateEnd } = this.frmReport.value
        let params = { promotor_id: promotorId, status: status, date_order_ini: dateStart, date_order_end: dateEnd, jwt: token }

        if (status == "") {
            delete params.status
        }

        if (this.userSession.is_admin == false) {
            Object.assign(params, { operator_id: this.userSession.related_id })
        }

        let paramsParse = this.httpHelper.parseParamsUrlGet(params)
        let url = GlobalVars.API_URL + "/orders/report/excel?" + paramsParse

        window.open(url, "_blank")
    }

    private showDetail(orderId) {
        this.router.navigate(['/deals/deal/' + orderId + '/detail'])
    }

    public changePage(page) {
        this.page = page
        this.searchDeals()
    }

    private showModalKiller() {
        $('#killerModal').modal('show')
    }

    private selectPromotor(promotor) {
        this.frmReport.patchValue({ killerName: promotor.name + " " + promotor.last_name, promotorId: promotor.id })
        $('#killerModal').modal('hide')
    }

    private searchKillers(searchValue: string) {
        this.promotorService.getAll({ name: searchValue, type_promotor: 'promotor' }).subscribe(
            (response) => {
                console.log("searchKiller", response)
                this.promotorList = response.data.results
            },
            (error) => {
                console.warn("searchKiller", error)
            }
        )
    }

    private searchDeals() {

        let { promotorId, status, dateStart, dateEnd } = this.frmReport.value
        let params = { promotor_id: promotorId, status: status, date_order_ini: dateStart, date_order_end: dateEnd, page_size: this.pageSize, page: this.page }

        if (this.userSession.is_admin == false) {
            Object.assign(params, { operator_id: this.userSession.related_id })
        }

        if (status == "") {
            delete params.status
        }

        this.orderService.getAll(params).subscribe(
            (response) => {
                this.dealList = response.data.results
                this.count = response.data.count
                console.warn("searchDeals", this.dealList)
            },
            (error) => {

            })

    }

    private createForm() {
        this.frmReport = this.frmBuilder.group({
            killerName: new FormControl(''),
            promotorId: 0,
            status: '',
            dateStart: moment().format("YYYY-MM-DD"),
            dateEnd: moment().format("YYYY-MM-DD")
        })

        this.searchField = new FormControl()
        this.searchField.valueChanges
            .pipe(
                debounceTime(500),
                distinctUntilChanged()
            )
            .subscribe((value) => {
                this.searchKillers(value)
            })
    }
}