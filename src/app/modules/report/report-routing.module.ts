import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'

import { ReportComponent } from './report.component'
import { ReportDealsListPage } from './pages/deals/list/report-deals-list'
import { ReportDealsKillerPage } from './pages/deals/killer/report-deals-killer'

const routes: Routes = [{
    path: '',
    component: ReportComponent,
    children: [

        {
            path: 'deals',
            component: ReportDealsListPage
        },
        {
            path: 'deals/killers',
            component: ReportDealsKillerPage
        },

    ],
},

    /* { path: '/config', redirectTo: '/config/operator' }, */
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class ReportRoutingModule {
}
