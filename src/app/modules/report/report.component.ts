import { Component } from '@angular/core'

declare var $: any
@Component({
    selector: 'module-report',
    //template: '<router-outlet></router-outlet>',
    template: '<theme-layout-main></theme-layout-main>'
})
export class ReportComponent {

    constructor() {

    }

    ngOnInit() {
        $('body').addClass("vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar")
    }
}
