import { NgModule } from '@angular/core';

import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http'

import { ThemeModule } from '../../theme/theme.module'
import { CitiesListPage } from './pages/cities/list/cities-list.page'
import { CitiesAddPage } from './pages/cities/add/cities-add.page';
import { CountryListPage } from './pages/country/list/country-list.page'
import { CountryAddPage } from './pages/country/add/country-add.page'
import { OperatorPage } from './pages/operator/list/operator.page'
import { ConfigComponent } from './config.component'
import { ConfigRoutingModule } from './config-routing.module'
import { OperatorAddPage } from './pages/operator/add/operator-add.page'
import { CatalogAddPage } from './pages/catalog/add/catalog-add.page'
import { CatalogListPage } from './pages/catalog/list/catalog-list.page'
import { CommonModule } from '../../../../node_modules/@angular/common'
import { BooleanTextPipe } from './pipes/BooleanTextPipe'

import { StatusAvatarPipe } from './pipes/StatusAvatarPipe'
import { HotelListPage } from './pages/hotel/list/hotel-list.page'
import { HotelAddPage } from './pages/hotel/add/hotel-add.page'

import { CategoryPage } from './pages/category/list/category.page'
import { CategoryAddPage } from './pages/category/add/category-add.page'

import { UserPage } from './pages/user/list/user.page'
import { UserAddPage } from './pages/user/add/user-add.page'
import { PackagePage } from './pages/package/list/package.page'
import { PackageAddPage } from './pages/package/add/package-add.page'

import { KcoinsAddPage } from './pages/kcoins/add/kcoins-add.page'
import { KcoinsListPage } from './pages/kcoins/list/kcoins-list.page'
import { MoneyListPage } from './pages/money/list/money-list.page'
import { MoneyAddPage } from './pages/money/add/money-add.page';
import { DivisaListPage } from './pages/divisa/list/divisa-list.page';
import { DivisaAddPage } from './pages/divisa/add/divisa-add.page';


const PAGES_COMPONENTS = [
    CitiesListPage,
    CitiesAddPage,
    CountryListPage,
    CountryAddPage,
    OperatorPage,
    OperatorAddPage,
    CatalogAddPage,
    CatalogListPage,
    ConfigComponent,
    HotelListPage,
    HotelAddPage,
    UserPage,
    UserAddPage,
    PackagePage,
    PackageAddPage,
    CategoryPage,
    CategoryAddPage,
    KcoinsAddPage,
    KcoinsListPage,
    MoneyListPage,
    MoneyAddPage,
    DivisaListPage,
    DivisaAddPage
];

const PIPES = [
    BooleanTextPipe,
    StatusAvatarPipe
]

@NgModule({
    imports: [
        CommonModule,
        ConfigRoutingModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule
    ],
    declarations: [
        ...PAGES_COMPONENTS,
        ...PIPES
    ],
    exports: [
        ...PIPES
    ],
    providers: [
    ]
})
export class ConfigModule {
}
