import { Component } from '@angular/core'
import { CountryService } from '../../../../../services/country.service'
import { Router, ActivatedRoute } from '@angular/router'

import { GlobalVars } from '../../../../../app.settings'
import { DivisaService } from 'src/app/services/divisa.service'

declare var swal: any

@Component({
    selector: 'app-divisa-list-page',
    templateUrl: './divisa-list.page.html',
    styleUrls: ['./divisa-list.page.scss']
})

export class DivisaListPage {
    private divisasList: Array<any> = Array<any>()
    private operatorId: number = 0

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private divisaService: DivisaService
    ) {

    }

    ngOnInit() {
        this.loadDivisas()
    }

    private loadDivisas() {
        this.operatorId = parseInt(this.route.snapshot.paramMap.get('id'))
        this.divisaService.getAll({ operator_id: this.operatorId, page_size: 20 }).subscribe(
            (response) => {
                this.divisasList = response.data.results
            },
            (error) => {

            }
        )
    }

    private delete(divisaId) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar esta divisa?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.divisaService.delete(divisaId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                        this.divisasList.map((divisa, key) => {
                            if (divisa.id == divisaId) {
                                this.divisasList.splice(key, 1)
                            }
                        })
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private update(divisaId) {
        this.router.navigate(['/config/operator/' + this.operatorId + '/divisas/' + divisaId + '/edit'])
    }
}
