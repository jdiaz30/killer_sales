import { Component } from '@angular/core'
import { CountryService } from '../../../../../services/country.service'
import { Router, ActivatedRoute } from '@angular/router'

import { GlobalVars } from '../../../../../app.settings'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { MoneyService } from 'src/app/services/money.service'
import { DivisaService } from 'src/app/services/divisa.service';

declare var swal: any
declare var $: any

@Component({
    selector: 'app-divisa-add-page',
    templateUrl: './divisa-add.page.html',
    styleUrls: ['./divisa-add.page.scss']
})

export class DivisaAddPage {
    private backLink: string = ""
    private moneyList: Array<any> = new Array<any>()
    private frmDivisa: FormGroup
    private validateForm: boolean = true
    private operatorId: number = 0
    private divisaId: number = 0

    constructor(
        private router: Router,
        private frmBuilder: FormBuilder,
        private route: ActivatedRoute,
        private moneyService: MoneyService,
        private divisaService: DivisaService
    ) {

    }

    ngOnInit() {
        this.operatorId = parseInt(this.route.snapshot.paramMap.get('id'))
        let divisaId = (this.route.snapshot.paramMap.get('divisaId') !== null) ? this.route.snapshot.paramMap.get('divisaId') : "0"
        this.divisaId = parseInt(divisaId)
        this.backLink = "/config/operator/" + this.operatorId + "/divisas"

        this.createForm()
        this.loadMoneyList()

        if (this.divisaId !== null && this.divisaId !== 0) {
            this.getDivisa(this.divisaId)
        }
    }

    private getDivisa(divisaId: number) {
        this.divisaService.get(divisaId).subscribe(
            (response) => {
                let data = response.data
                this.frmDivisa.patchValue({
                    money_a_id: data.money_a_id,
                    money_b_id: data.money_b_id,
                    status: data.status,
                    money_a_value: data.money_a_value,
                    money_b_value: data.money_b_value,
                    operator_id: data.operator_id
                })
            },
            (error) => {

            }
        )
    }

    private save() {
        if (this.frmDivisa.valid) {
            if (this.divisaId == 0) {
                this.create(this.frmDivisa.value)
            } else {
                this.update(this.divisaId, this.frmDivisa.value)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "warning")
        }
    }

    private create(data: any) {
        this.divisaService.create(data).subscribe(
            (response) => {
                swal("Alert", "Se registro su divisa", "success")
                this.router.navigate([this.backLink])
            },
            (error) => {
                swal("Alert", "No se pudo registrar su divisa", "warning")
            })
    }

    private update(divisaId: number, data: any) {
        this.divisaService.update(data, divisaId).subscribe(
            (response) => {
                swal("Alert", "Se actualizo su divisa", "success")
                this.router.navigate([this.backLink])
            },
            (error) => {
                swal("Alert", "No se pudo actualizar su divisa", "warning")
            })
    }

    private changeMonedaB() {
        let { money_a_id, money_b_id } = this.frmDivisa.value
        if (money_a_id == money_b_id) {
            swal("Alert", "Las monedas tienen que ser diferentes", "info")
        } else {
            if (money_a_id == 0 || money_b_id == 0) {
                swal("Alert", "Tiene que seleccionar una moneda", "info")
            }
        }
    }

    private createForm() {
        this.frmDivisa = this.frmBuilder.group({
            money_a_id: new FormControl(0, [<any>Validators.required]),
            money_b_id: new FormControl('0', [<any>Validators.required]),
            money_a_value: new FormControl('', [<any>Validators.required]),
            money_b_value: new FormControl('', [<any>Validators.required]),
            operator_id: this.operatorId,
            status: 1
        })
    }

    private loadMoneyList() {
        this.moneyService.getAll({ page_size: 100, status: 1 }).subscribe(
            (response) => {
                this.moneyList = response.data.results
            },
            (error) => {

            }
        )
    }

}
