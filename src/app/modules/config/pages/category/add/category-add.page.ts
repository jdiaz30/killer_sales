import { Component } from '@angular/core';
import { CategoryService } from '../../../../../services/category.service';
import { UserSessionHelper } from '../../../../../helpers/user-session.helper';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ParamMap, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';

declare var swal: any

@Component({
    selector: 'app-category-add-page',
    templateUrl: './category-add.page.html',
    styleUrls: ['./category-add.page.scss']
})

export class CategoryAddPage {

    title = 'Paquete';
    private operatorsList: Array<any> = new Array<any>()
    private frmPackage: FormGroup
    private isUpdate: boolean = false
    private categoryId: number = 0

    constructor(
        private categoryService: CategoryService,
        private userSessionHelper: UserSessionHelper,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.createForm()
        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getCategory(id)
            this.isUpdate = true
            this.categoryId = parseInt(id)
        }
    }

    private getCategory(categoryId) {
        this.categoryService.get(categoryId).subscribe(
            (response) => {
                let data = response.data
                this.frmPackage.patchValue({
                    name: data.name,
                    status: data.status
                })
            },
            (error) => {

            }
        )
    }

    private createForm() {
        this.frmPackage = this.frmBuilder.group({
            name: new FormControl('', [<any>Validators.required]),
            status: 1
        })
    }

    private save() {
        if (this.frmPackage.valid == true) {
            if (this.isUpdate == false) {
                this.create(this.frmPackage.value)
            } else {
                this.update(this.frmPackage.value)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.categoryService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/package/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.categoryService.update(data, this.categoryId, {}).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/package/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}

