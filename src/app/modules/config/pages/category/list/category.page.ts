import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../../../../../services/category.service';

declare var swal: any

@Component({
    selector: 'app-category-page',
    templateUrl: './category.page.html',
    styleUrls: ['./category.page.scss']
})

export class CategoryPage {

    title = 'Categoria';

    private categoryList: Array<any> = new Array<any>()
    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''

    constructor(
        private categoryService: CategoryService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list();
    }

    public changePage(page) {
        this.page = page
        this.list()
    }

    public search(value) {
        this.list()
    }

    private list() {
        this.categoryService.getAll({ page_size: this.pageSize, page: this.page, name: this.testSearch }).subscribe(
            (response) => {
                this.categoryList = response.data.results
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private delete(id, index) {

        let self = this
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar esta categoria?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.categoryService.delete(id).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                        self.categoryList.splice(index, 1);
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }
        }).catch(swal.noop)
    }



    private update(id) {
        this.router.navigate(['/config/category/' + id + '/edit']);
    }

    private changeStatus(item, index) {
        let title = (item.status === 1) ? 'Desactivar Categoría' : 'Activar Categoría'
        let text = (item.status === 1) ? ('¿Esta seguro de desactivar la categoría ' + item.user_name + '?') : ('¿Esta seguro de activar la categoría ' + item.user_name + '?')
        let status = (item.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.categoryService.update({ status: status }, item.id, {}).subscribe(
                    (response) => {
                        this.categoryList[index] ? this.categoryList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
    }
}
