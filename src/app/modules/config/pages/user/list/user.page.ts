import { Component } from '@angular/core';
import { UserService } from '../../../../../services/user.service';
import { Router } from '@angular/router';

declare var swal: any

@Component({
    selector: 'app-user-page',
    templateUrl: './user.page.html',
    styleUrls: ['./user.page.scss']
})

export class UserPage {

    title = 'Usuario';

    private userList: Array<any> = new Array<any>()
    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''

    constructor(
        private userService: UserService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list()
    }

    public changePage(page) {
        this.page = page
        this.list()
    }

    public search(value) {
        this.list()
    }

    private list() {
        this.userService.getAll({ page_size: this.pageSize, page: this.page, user_name: this.testSearch }).subscribe(
            (response) => {
                this.userList = response.data.results.filter((user) => user.type_user !== "admin")
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private deleteOperator(userId) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este operador?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.userService.delete(userId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private updateOperator(userId) {
        this.router.navigate(['/config/user/' + userId + '/edit'])
    }

    private changeStatus(item, index) {
        let title = (item.status === 1) ? 'Desactivar User' : 'Activar User'
        let text = (item.status === 1) ? ('¿Esta seguro de desactivar al user ' + item.user_name + '?') : ('¿Esta seguro de activar al user ' + item.user_name + '?')
        let status = (item.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.userService.update({ status: status }, item.id).subscribe(
                    (response) => {
                        this.userList[index] ? this.userList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
    }
}
