import { Component } from '@angular/core';
import { UserService } from '../../../../../services/user.service';
import { UserSessionHelper } from '../../../../../helpers/user-session.helper';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ParamMap, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { OperatorService } from 'src/app/services/operator.service';
import { PromotorService } from 'src/app/services/promotor.service';

declare var swal: any

@Component({
    selector: 'app-user-add-page',
    templateUrl: './user-add.page.html',
    styleUrls: ['./user-add.page.scss']
})

export class UserAddPage {

    title = 'Usuario'
    private frmUser: FormGroup
    private frmUserFilter: FormGroup
    private isUpdate: boolean = false
    private userId: number = 0
    private usersFilters: Array<any> = new Array<any>()
    private relatedUser: any = null
    private userData: any = null

    constructor(
        private userService: UserService,
        private userSessionHelper: UserSessionHelper,
        private operatorService: OperatorService,
        private promotorService: PromotorService,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.createForm()
        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getUser(id)
            this.isUpdate = true
            this.userId = parseInt(id)
        }
    }

    private handleSelectRelatedUser(relatedUser: any) {
        let textAlert = (this.frmUserFilter.value.type_user == "operator") ? "Operador" : "Promotor"
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de seleccionar este ' + textAlert + '?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.relatedUser = { ...relatedUser }
                console.log("relatedUser", this.relatedUser)
                if (this.isUpdate == true) {
                    let data = { related_id: relatedUser.id, type_user: this.frmUserFilter.value.type_user }
                    this.update(data)
                } else {
                    this.frmUser.patchValue({ type_user: this.frmUserFilter.value.type_user })
                }
            }
        }).catch(swal.noop)
    }

    private search(event) {
        if (this.frmUserFilter.value.type_user !== "0") {
            let { type_user, name } = this.frmUserFilter.value
            if (type_user == "promotor" || type_user == "cobrator") {
                this.searchPromotor(name, type_user)
            } else {
                this.searchOperator(name)
            }
        } else {
            this.usersFilters.length = 0
        }
    }

    private searchOperator(name: string) {
        this.operatorService.getAll({ name: name, page_size: 10 }).subscribe(
            (response) => {
                this.usersFilters = response.data.results
            },
            (error) => {
                console.warn("searchOperator", error)
            })
    }

    private searchPromotor(name: string, typePromotor: string) {
        this.promotorService.getAll({ name: name, type_promotor: typePromotor, page_size: 10 }).subscribe(
            (response) => {
                this.usersFilters = response.data.results
            }
            ,
            (error) => {
                console.warn("searchPromotor", error)
            })
    }

    private getUser(userId) {
        this.userService.get(userId).subscribe(
            (response) => {
                this.userData = response.data
                this.frmUser.patchValue({
                    user_name: this.userData.user_name,
                    type_user: this.userData.type_user,
                    is_admin: this.userData.is_admin
                })

                this.frmUserFilter.patchValue({ type_user: this.userData.type_user })

                if (this.userData.type_user == "operator") {
                    this.operatorService.get(this.userData.related_id).subscribe(
                        (response) => {
                            this.relatedUser = response.data
                            console.log("operatorService", response.data)
                        },
                        (error) => {

                        })
                } else if (this.userData.type_user == "promotor" || this.userData.type_user == "cobrator") {
                    this.promotorService.get(this.userData.related_id).subscribe(
                        (response) => {
                            this.relatedUser = response.data
                            console.log("promotorService", response.data)
                        },
                        (error) => {

                        }

                    )
                }
            },
            (error) => {

            }
        )
    }

    private handleIsAdmin() {
        this.frmUser.patchValue({ type_user: this.frmUserFilter.value.type_user })
    }

    private createForm() {
        this.frmUser = this.frmBuilder.group({
            user_name: new FormControl('', [<any>Validators.required]),
            type_user: new FormControl('0'),
            is_admin: new FormControl(false),
            password: new FormControl(''),
            status: 1
        })

        this.frmUserFilter = this.frmBuilder.group({
            name: new FormControl(''),
            type_user: new FormControl('0'),
        })
    }

    private save() {
        let data = {
            user_name: this.frmUser.value.user_name,
            type_user: (this.frmUserFilter.value.type_user == "0" && this.frmUser.value.is_admin == true) ? "0" : this.frmUserFilter.value.type_user,
            is_admin: this.frmUser.value.is_admin,
            password: this.frmUser.value.password,
            status: this.frmUser.value.status,
            related_id: (this.relatedUser !== null) ? this.relatedUser.id : 0
        }

        if (data.password == "") {
            delete data.password
        }

        if (this.frmUser.valid == true) {
            if (this.isUpdate == false) {
                this.create(data)
            } else {
                this.update(data)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.userService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/user/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.userService.update(data, this.userId).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/user/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}
