import { Component } from '@angular/core';
import { KcoinsService } from '../../../../../services/kcoins.service';
import { Router } from '@angular/router';

import { GlobalVars } from '../../../../../app.settings';

declare var swal: any;

@Component({
    selector: 'app-kcoins-page',
    templateUrl: './kcoins-list.page.html',
    styleUrls: ['./kcoins-list.page.scss']
})

export class KcoinsListPage {

    title = 'Kcoins';
    private kcoinsList: Array<any> = new Array<any>()
    private urlBaseImage = GlobalVars.API_BASE_URL
    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''
    private type_pax: string = ''

    constructor(
        private kcoinsService: KcoinsService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list()
    }

    public changePage(page) {
        this.page = page
        this.list()
    }

    public search(value) {
        this.list()
    }

    private list() {
        var filters = { page_size: this.pageSize, page: this.page, name: this.testSearch }
        if (this.type_pax != '')
            filters['type_pax'] = this.type_pax

        this.kcoinsService.getAll(filters).subscribe(
            (response) => {
                this.kcoinsList = response.data.results
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private update(catalogId) {
        this.router.navigate(['/config/kcoins/' + catalogId + '/edit']);
    }

    private delete(catalogId) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este país?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.kcoinsService.delete(catalogId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                        this.kcoinsList.map((catalog, key) => {
                            if (catalog.id == catalogId) {
                                this.kcoinsList.splice(key, 1)
                            }
                        })
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private changeStatus(item, index) {
        let title = (item.status === 1) ? 'Desactivar Kcoin' : 'Activar Kcoin'
        let text = (item.status === 1) ? ('¿Esta seguro de desactivar kcoin ' + item.name + '?') : ('¿Esta seguro de activar al país ' + item.name + '?')
        let status = (item.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.kcoinsService.update({ status: status }, item.id).subscribe(
                    (response) => {
                        this.kcoinsList[index] ? this.kcoinsList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
    }

}
