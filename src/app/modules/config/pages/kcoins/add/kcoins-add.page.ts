import { Component } from '@angular/core';
import { UserSessionHelper } from '../../../../../helpers/user-session.helper';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalVars } from '../../../../../app.settings';
import { KcoinsService } from '../../../../../services/kcoins.service';

declare var swal: any
declare var $: any

@Component({
    selector: 'app-kcoins-add-page',
    templateUrl: './kcoins-add.page.html',
    styleUrls: ['./kcoins-add.page.scss']
})

export class KcoinsAddPage {

    private frmKcoins: FormGroup
    private isUpdate: boolean = false
    private avatar: any = null
    private kcoinsId: number = 0

    constructor(
        private kcoinsService: KcoinsService,
        private userSessionHelper: UserSessionHelper,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.createForm()

        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getAvatar(id)
            this.isUpdate = true
            this.kcoinsId = parseInt(id)
        }
    }

    private getAvatar(kcoinsId) {
        this.kcoinsService.get(kcoinsId).subscribe(
            (response) => {
                let data = response.data
                this.frmKcoins.patchValue({ pax: data.pax, type_pax: data.type_pax, kcoins: data.kcoins })
            },
            (error) => {

            }
        )
    }

    private createForm() {
        this.frmKcoins = this.frmBuilder.group({
            pax: new FormControl('', [<any>Validators.required]),
            type_pax: new FormControl('', [<any>Validators.required]),
            kcoins: new FormControl('', [<any>Validators.required]),
            //status: new FormControl(1)
        })
    }

    cancel() {
        this.router.navigate(['/config/kcoins']);
    }

    private save() {
        if (this.frmKcoins.valid == true) {
            if (this.isUpdate == false) {
                this.create(this.frmKcoins.value)
            } else {
                this.update(this.frmKcoins.value)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.kcoinsService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/kcoins']);
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.kcoinsService.update(data, this.kcoinsId).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/kcoins']);
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}
