import { Component } from '@angular/core';
import { CountryService } from '../../../../../services/country.service';
import { Router } from '@angular/router';

import { GlobalVars } from '../../../../../app.settings';

declare var swal: any;

@Component({
    selector: 'app-country-page',
    templateUrl: './country-list.page.html',
    styleUrls: ['./country-list.page.scss']
})

export class CountryListPage {

    title = 'Ciudades';
    private countryList: Array<any> = new Array<any>()
    private urlBaseImage = GlobalVars.API_BASE_URL
    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''

    constructor(
        private countryService: CountryService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list()
    }

    public changePage(page) {
        this.page = page
        this.list()
    }

    public search(value) {
        this.list()
    }

    private list() {
        this.countryService.getAll({ page_size: this.pageSize, page: this.page, name: this.testSearch }).subscribe(
            (response) => {
                this.countryList = response.data.results
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private update(catalogId) {
        this.router.navigate(['/config/country/' + catalogId + '/edit']);
    }

    private delete(catalogId) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este país?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.countryService.delete(catalogId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                        this.countryList.map((catalog, key) => {
                            if (catalog.id == catalogId) {
                                this.countryList.splice(key, 1)
                            }
                        })
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private changeStatus(catalog, index) {
        let title = (catalog.status === 1) ? 'Desactivar País' : 'Activar País'
        let text = (catalog.status === 1) ? ('¿Esta seguro de desactivar al país ' + catalog.name + '?') : ('¿Esta seguro de activar al país ' + catalog.name + '?')
        let status = (catalog.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.countryService.update({ status: status }, catalog.id).subscribe(
                    (response) => {
                        this.countryList[index] ? this.countryList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
    }
}
