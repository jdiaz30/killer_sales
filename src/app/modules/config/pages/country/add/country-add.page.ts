import { Component } from '@angular/core';
import { UserSessionHelper } from '../../../../../helpers/user-session.helper';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalVars } from '../../../../../app.settings';
import { CountryService } from '../../../../../services/country.service';

declare var swal: any
declare var $: any

@Component({
    selector: 'app-country-add-page',
    templateUrl: './country-add.page.html',
    styleUrls: ['./country-add.page.scss']
})

export class CountryAddPage {

    private frmCountry: FormGroup
    private isUpdate: boolean = false
    private avatar: any = null
    private countryId: number = 0

    constructor(
        private countryService: CountryService,
        private userSessionHelper: UserSessionHelper,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.createForm()

        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getCountry(id)
            this.isUpdate = true
            this.countryId = parseInt(id)
        }
    }

    private getCountry(countryId) {
        this.countryService.get(countryId).subscribe(
            (response) => {
                let data = response.data
                this.frmCountry.patchValue({ name: data.name, iso: data.iso, number_area: data.number_area, status: data.status })
            },
            (error) => {

            }
        )
    }

    private createForm() {
        this.frmCountry = this.frmBuilder.group({
            name: new FormControl('', [<any>Validators.required]),
            iso: new FormControl('', [<any>Validators.required]),
            number_area: new FormControl('', [<any>Validators.required]),
            status: new FormControl(1)
        })
    }

    cancel() {
        this.router.navigate(['/config/country'])
    }

    private save() {
        if (this.frmCountry.valid == true) {
            if (this.isUpdate == false) {
                this.create(this.frmCountry.value)
            } else {
                this.update(this.frmCountry.value)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.countryService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.cancel()
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.countryService.update(data, this.countryId).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.cancel()
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}
