import { Component } from '@angular/core';
import { CitieService } from '../../../../../services/citie.service';
import { UserSessionHelper } from '../../../../../helpers/user-session.helper';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalVars } from '../../../../../app.settings';

declare var swal: any
declare var $: any

@Component({
    selector: 'app-cities-add-page',
    templateUrl: './cities-add.page.html',
    styleUrls: ['./cities-add.page.scss']
})

export class CitiesAddPage {

    private frmCitie: FormGroup
    private isUpdate: boolean = false
    private avatar: any = null
    private cityId: number = 0

    constructor(
        private citieService: CitieService,
        private userSessionHelper: UserSessionHelper,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.createForm()

        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getCity(id)

            this.isUpdate = true
            this.cityId = parseInt(id)
        }
    }

    private getCity(cityId) {
        this.citieService.get(cityId).subscribe(
            (response) => {
                let data = response.data
                this.frmCitie.patchValue({ name: data.name, slug: data.slug, status: data.status })
            },
            (error) => {

            }
        )
    }

    private createForm() {
        this.frmCitie = this.frmBuilder.group({
            name: new FormControl('', [<any>Validators.required]),
            slug: new FormControl('', [<any>Validators.required]),
            status: new FormControl(1)
        })
    }

    cancel() {
        this.router.navigate(['/config/cities'])
    }

    private save() {
        if (this.frmCitie.valid == true) {
            if (this.isUpdate == false) {
                this.create(this.frmCitie.value)
            } else {
                this.update(this.frmCitie.value)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.citieService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.cancel()
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.citieService.update(data, this.cityId).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.cancel()
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}
