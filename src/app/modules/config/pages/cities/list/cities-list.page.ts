import { Component } from '@angular/core';
import { CitieService } from '../../../../../services/citie.service';
import { Router } from '@angular/router';

import { GlobalVars } from '../../../../../app.settings';

declare var swal: any;

@Component({
    selector: 'app-cities-page',
    templateUrl: './cities-list.page.html',
    styleUrls: ['./cities-list.page.scss']
})

export class CitiesListPage {

    title = 'Ciudades';
    private citieList: Array<any> = new Array<any>()
    private urlBaseImage = GlobalVars.API_BASE_URL
    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''

    constructor(
        private citieService: CitieService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list()
    }

    public changePage(page) {
        this.page = page
        this.list()
    }

    public search(value) {
        this.list()
    }

    private list() {
        this.citieService.getAll({ page_size: this.pageSize, page: this.page, name: this.testSearch }).subscribe(
            (response) => {
                this.citieList = response.data.results
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private update(catalogId) {
        this.router.navigate(['/config/cities/' + catalogId + '/edit']);
    }

    private delete(catalogId) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este catálogo?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.citieService.delete(catalogId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                        this.citieList.map((catalog, key) => {
                            if (catalog.id == catalogId) {
                                this.citieList.splice(key, 1)
                            }
                        })
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private changeStatus(city, index) {
        let title = (city.status === 1) ? 'Desactivar Ciudad' : 'Activar Ciudad'
        let text = (city.status === 1) ? ('¿Esta seguro de desactivar al ciudad ' + city.name + '?') : ('¿Esta seguro de activar al ciudad ' + city.name + '?')
        let status = (city.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.citieService.update({ status: status }, city.id).subscribe(
                    (response) => {
                        this.citieList[index] ? this.citieList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
    }
}
