import { Component } from '@angular/core'
import { CatalogService } from '../../../../../services/catalog.service'
import { UserSessionHelper } from '../../../../../helpers/user-session.helper'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { OperatorService } from '../../../../../services/operator.service'
import { GlobalVars } from '../../../../../app.settings';
import { ActivityService } from '../../../../../services/activity.service'
import { CitieService } from '../../../../../services/citie.service'
import { CategoryService } from 'src/app/services/category.service'

declare var swal: any
declare var $: any

@Component({
    selector: 'app-catalog-add-page',
    templateUrl: './catalog-add.page.html',
    styleUrls: ['./catalog-add.page.scss']
})

export class CatalogAddPage {
    title = 'Catalogo'

    private urlBaseImage = GlobalVars.API_BASE_URL
    private frmCatalog: FormGroup
    private frmActivity: FormGroup
    private operatorsList: Array<any> = new Array<any>()
    private categoryList: Array<any> = new Array<any>()
    private catalogsList: Array<any> = new Array<any>()
    private isUpdate: boolean = false
    private poster: any = null
    private posterActivity: any = null
    private catalogId: number = null
    private activityList: Array<any> = new Array<any>()
    private citiesList: Array<any> = new Array<any>()
    private actIdSelected: number = 0
    private userSession: any

    constructor(
        private catalogService: CatalogService,
        private operatorService: OperatorService,
        private activityService: ActivityService,
        private cityService: CitieService,
        private userSessionHelper: UserSessionHelper,
        private categoryService: CategoryService,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.userSession = this.userSessionHelper.getUser()
        this.createForm()
        this.listOperator()
        this.listCities()
        this.listCategories()

        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getCatalog(id)
            this.getCatalogActivities(id)
            this.isUpdate = true
            this.catalogId = parseInt(id)
        }
    }

    private closeActivity() {
        this.actIdSelected = 0
        this.frmActivity.reset()
        this.frmActivity.patchValue({
            name: "",
            description: "",
            category_id: "",
            price_pub_adult: "",
            price_pub_child: "",
            price_rep_adult: "",
            price_rep_child: "",
            max_capacity_day: 0,
            city_id: "",
            is_transport: false,
            status: 1
        })
    }

    get pricePubAdult() {
        return this.frmActivity.get('price_pub_adult')
    }

    private cloneActivity(catalogId) {
        let activitiesId = this.activityList.filter((item) => item.selectedClone == true).map((item) => item.id)

        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de clonar las actividades hacia este catalogo?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.activityService.clone({ catalog_id: catalogId, activities_id: activitiesId }).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private filterCatalog(operatorId) {
        console.log("filterCatalog", operatorId)

        this.catalogService.getAll({ page_size: 100, operator_id: operatorId }).subscribe(
            (response) => {
                this.catalogsList = response.data.results
            },
            (error) => {

            }
        )
    }

    private editActivity(activity) {
        this.frmActivity.patchValue({
            name: activity.name,
            description: activity.description,
            category_id: activity.category_id,
            price_pub_adult: activity.price_pub_adult,
            price_pub_child: activity.price_pub_child,
            price_rep_adult: activity.price_rep_adult,
            price_rep_child: activity.price_rep_child,
            city_id: activity.city.id,
            is_transport: activity.is_transport,
            max_capacity_day: activity.max_capacity_day,
            status: 1
        })

        this.actIdSelected = activity.id

        //$("#activityForm").modal('show')
    }

    private addActivity() {
        if (this.frmActivity.valid == true) {
            let activityClone = { ...this.frmActivity.value }
            if (typeof this.posterActivity !== 'string') {
                activityClone.posterFile = this.posterActivity
            }
            let city = this.citiesList.filter((item) => item.id == this.frmActivity.value.city_id)
            activityClone.city = (city.length > 0) ? city[0] : { id: 1, name: "Ninguno" }
            activityClone.city_id = activityClone.city.id

            if (this.actIdSelected == 0) {
                this.activityList.push(activityClone)
            } else {

                this.activityList.map((activity) => {

                    if (activity.id == this.actIdSelected) {

                        activity.name = this.frmActivity.value.name
                        activity.description = this.frmActivity.value.description
                        activity.category_id = this.frmActivity.value.category_id
                        activity.price_pub_adult = this.frmActivity.value.price_pub_adult
                        activity.price_pub_child = this.frmActivity.value.price_pub_child
                        activity.price_rep_adult = this.frmActivity.value.price_rep_adult
                        activity.price_rep_child = this.frmActivity.value.price_rep_child
                        activity.max_capacity_day = this.frmActivity.value.max_capacity_day
                        activity.is_transport = this.frmActivity.value.is_transport

                        activity.city = activityClone.city
                        activity.city_id = activityClone.city.id
                        activity.id = this.actIdSelected
                        activity.status = 1

                        if (activityClone.posterFile)
                            activity.posterFile = activityClone.posterFile

                    }
                    return activity
                })

            }

            this.closeActivity()
            $("#activityForm").modal('hide')

        } else {
            swal("Error", "Ingrese datos", "warning")
        }

        this.actIdSelected = 0
    }

    private deleteActivity(activity) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar esta actividad?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.activityList.filter((item) => activity.id == item.id).map((item) => item.deleted = true)
            }
        }).catch(swal.noop)
    }

    private getCatalog(catalogId) {
        this.catalogService.get(catalogId).subscribe(
            (response) => {
                let data = response.data
                this.frmCatalog.patchValue({ name: data.name, operator_id: data.operator_id, status: data.status })
                if (data.poster !== null) {
                    let urlPoster = GlobalVars.API_BASE_URL + data.poster
                    this.setImage(urlPoster, "url")
                }
            },
            (error) => {

            }
        )
    }

    private getCatalogActivities(catalogId) {
        this.catalogService.getAllActivities(catalogId).subscribe(
            (response) => {
                let activities = response.data
                activities.map((activity) => {
                    this.activityList.push({
                        catalog_id: activity.catalog_id,
                        description: activity.description,
                        category_id: activity.category_id,
                        name: activity.name,
                        price_pub_adult: activity.price_pub_adult,
                        price_pub_child: activity.price_pub_child,
                        price_rep_adult: activity.price_rep_adult,
                        price_rep_child: activity.price_rep_child,
                        city: activity.city,
                        city_id: activity.city.id,
                        is_transport: activity.is_transport,
                        id: activity.id,
                        status: activity.status,
                        max_capacity_day: activity.max_capacity_day,
                        poster: activity.poster,
                        poster_500: activity.poster_500
                    })
                })
            },
            (error) => {

            }
        )
    }

    private createForm() {
        this.frmCatalog = this.frmBuilder.group({
            name: new FormControl('', [<any>Validators.required]),
            operator_id: new FormControl('0'),
            status: 1
        })

        let pricePattern = "^\\d+\\.\\d{0,2}$"

        this.frmActivity = this.frmBuilder.group({
            name: new FormControl('', [<any>Validators.required]),
            description: new FormControl(''),
            price_pub_adult: new FormControl('', [<any>Validators.required]),
            price_pub_child: new FormControl('', [<any>Validators.required]),
            price_rep_adult: new FormControl('', [<any>Validators.required]),
            price_rep_child: new FormControl('', [<any>Validators.required]),
            city_id: new FormControl(''),
            category_id: new FormControl(''),
            is_transport: new FormControl(false),
            max_capacity_day: new FormControl(0),
            status: 1
        })
    }

    private onFileChange(event: any) {
        let files = event.target.files
        let data = {
            file: files[0],
            title: "photo "
        }
        this.poster = files[0]
        this.setImage(files[0], "object")
    }

    private onFileActivityChange(event: any) {
        let files = event.target.files
        let data = {
            file: files[0],
            title: "photo "
        }
        this.posterActivity = files[0]
        this.setImageActivity(files[0], "object")
    }

    private setImage(urlImage, type) {
        let image = (type == "object") ? URL.createObjectURL(urlImage) : urlImage
        $("#avatar").attr("src", image)
    }

    private setImageActivity(urlImage, type) {
        let image = (type == "object") ? URL.createObjectURL(urlImage) : urlImage
        $("#avatarActivity").attr("src", image)
    }

    private listOperator() {
        let params = { page_size: 100, status: 1 }

        if (this.userSession.type_user == "operator") {
            params = Object.assign(params, { operator_id: this.userSession.related_id })
            this.frmCatalog.patchValue({ operator_id: params['operator_id'] })
        }

        this.operatorService.getAll(params).subscribe(
            (response) => {
                this.operatorsList = response.data.results
            },
            (error) => {

            }
        )
    }

    private showCloneModal() {
        let idsClone = this.activityList.filter((item) => item.selectedClone == true)
        if (idsClone.length > 0) {
            $("#activityClone").modal('show')
        } else {
            swal("Aviso", "Seleccione actividades para clonación", "info")
        }
    }

    private selectedCloneActivity(activity) {
        this.activityList.map((item) => {
            if (item.id == activity.id) {
                console.log("entre aqui", item.selectedClone)
                item.selectedClone = item.selectedClone == false || item.selectedClone == undefined ? true : false
            }
            return item
        })
    }

    private listCategories() {
        this.categoryService.getAll({ page_size: 30, status: 1 }).subscribe(
            (response) => {
                this.categoryList = response.data.results
            },
            (error) => {

            }
        )
    }

    private listCities() {
        this, this.cityService.getAll({ page_size: 30, status: 1 }).subscribe(
            (response) => {
                this.citiesList = response.data.results
            }
        )
    }

    private save() {
        if (this.frmCatalog.valid == true) {
            let data = {
                name: ""
            }

            let formData: FormData = new FormData()
            formData.append("name", this.frmCatalog.value.name)
            formData.append("status", "1")
            formData.append("operator_id", this.frmCatalog.value.operator_id)

            if (this.poster !== null) {
                formData.append("poster", this.poster)
            }

            if (this.isUpdate == false) {
                this.create(formData)
            } else {
                this.update(formData)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private saveActivity(catalogId, option) {

        let formData: FormData = new FormData()

        this.activityList.forEach((activity, index) => {

            formData.append("[" + index + "]catalog_id", catalogId + '')
            formData.append("[" + index + "]name", activity.name)
            formData.append("[" + index + "]description", activity.description)
            formData.append("[" + index + "]price_pub_adult", activity.price_pub_adult)
            formData.append("[" + index + "]price_pub_child", activity.price_pub_child)
            formData.append("[" + index + "]price_rep_adult", activity.price_rep_adult)
            formData.append("[" + index + "]price_rep_child", activity.price_rep_child)
            formData.append("[" + index + "]city_id", activity.city_id)
            formData.append("[" + index + "]category_id", activity.category_id)
            formData.append("[" + index + "]is_transport", activity.is_transport)
            formData.append("[" + index + "]status", activity.status)
            formData.append("[" + index + "]max_capacity_day", activity.max_capacity_day)

            if (activity.posterFile && activity.posterFile !== null) {
                formData.append("[" + index + "]poster", activity.posterFile)
            }

        })

        this.activityService.save(formData, option).subscribe(
            (response) => {

            },
            (error) => {

            }
        )
    }

    private updateActivity(catalogId, option) {

        this.activityList.forEach((activity, index) => {

            let formData: FormData = new FormData()

            formData.append("catalog_id", catalogId + '')
            formData.append("name", activity.name)
            formData.append("description", activity.description)
            formData.append("price_pub_adult", activity.price_pub_adult)
            formData.append("price_pub_child", activity.price_pub_child)
            formData.append("price_rep_adult", activity.price_rep_adult)
            formData.append("price_rep_child", activity.price_rep_child)
            formData.append("city_id", activity.city_id)
            formData.append("category_id", activity.category_id)
            formData.append("is_transport", activity.is_transport)
            formData.append("status", activity.status)
            formData.append("max_capacity_day", activity.max_capacity_day)

            if (activity.posterFile && activity.posterFile !== null) {
                formData.append("poster", activity.posterFile)
            }

            if (activity.id) {
                formData.append("id", activity.id)
            }

            if (activity.deleted) {
                formData.append("deleted", activity.deleted)
            }

            this.activityService.save(formData, option).subscribe(
                (response) => {

                },
                (error) => {

                }
            )
        })


    }

    private create(data) {
        this.catalogService.save(data).subscribe(
            (response) => {
                this.saveActivity(response.data.id, { option: "create" })
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/catalog/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.catalogService.update(data, this.catalogId).subscribe(
            (response) => {
                this.updateActivity(response.data.id, { option: "update" })
                swal("Éxito", response.user_msg, "success")

                this.router.navigate(['/config/catalog/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}
