import { Component } from '@angular/core'
import { CatalogService } from '../../../../../services/catalog.service'
import { Router } from '@angular/router'

import { GlobalVars } from '../../../../../app.settings'
import { UserSessionHelper } from 'src/app/helpers/user-session.helper'

declare var swal: any

@Component({
    selector: 'app-catalog-page',
    templateUrl: './catalog-list.page.html',
    styleUrls: ['./catalog-list.page.scss']
})

export class CatalogListPage {
    title = 'Catalogo';
    private catalogList: Array<any> = new Array<any>()
    private urlBaseImage = GlobalVars.API_BASE_URL
    private userSession: any

    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''

    constructor(
        private catalogService: CatalogService,
        private userSessionHelper: UserSessionHelper,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.userSession = this.userSessionHelper.getUser()
        this.listCatalog()
    }

    public changePage(page) {
        this.page = page
        this.listCatalog()
    }

    public search(value) {
        this.listCatalog()
    }

    private listCatalog() {
        let params = { page_size: this.pageSize, page: this.page, name: this.testSearch }

        if (this.userSession.type_user == "operator") {
            params = Object.assign(params, { operator_id: this.userSession.related_id })
        }

        this.catalogService.getAll(params).subscribe(
            (response) => {
                this.catalogList = response.data.results
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private updateCatalog(catalogId) {
        this.router.navigate(['/config/catalog/' + catalogId + '/edit']);
    }

    private deleteCatalog(catalogId, index) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este catálogo?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.catalogService.delete(catalogId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                        this.catalogList.splice(index, 1)
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private changeStatus(catalog, index) {
        let title = (catalog.status === 1) ? 'Desactivar Catálogo' : 'Activar Catálogo'
        let text = (catalog.status === 1) ? ('¿Esta seguro de desactivar al catálogo ' + catalog.name + '?') : ('¿Esta seguro de activar al catálogo ' + catalog.name + '?')
        let status = (catalog.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.catalogService.update({ status: status }, catalog.id).subscribe(
                    (response) => {
                        this.catalogList[index] ? this.catalogList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
    }
}
