import { Component } from '@angular/core';
import { OperatorService } from '../../../../../services/operator.service';
import { Router } from '@angular/router';

declare var swal: any

@Component({
    selector: 'app-operator-page',
    templateUrl: './operator.page.html',
    styleUrls: ['./operator.page.scss']
})

export class OperatorPage {
    title = 'Operator';

    private operatorsList: Array<any> = new Array<any>()
    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''

    constructor(
        private operatorService: OperatorService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list()
    }

    public changePage(page) {
        this.page = page
        this.list()
    }

    public search(value) {
        this.list()
    }

    private list() {
        this.operatorService.getAll({ page_size: this.pageSize, page: this.page, name: this.testSearch }).subscribe(
            (response) => {
                this.operatorsList = response.data.results
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private deleteOperator(operatorId) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este operador?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.operatorService.delete(operatorId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private updateOperator(operatorId) {
        this.router.navigate(['/config/operator/' + operatorId + '/edit'])
    }

    private showDivisas(operatorId) {
        this.router.navigate(['/config/operator/' + operatorId + '/divisas'])
    }

    private changeStatus(item, index) {
        let title = (item.status === 1) ? 'Desactivar Operador' : 'Activar Operador'
        let text = (item.status === 1) ? ('¿Esta seguro de desactivar operador ' + item.name + '?') : ('¿Esta seguro de activar operador ' + item.name + '?')
        let status = (item.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.operatorService.update({ status: status }, item.id).subscribe(
                    (response) => {
                        this.operatorsList[index] ? this.operatorsList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
    }
}
