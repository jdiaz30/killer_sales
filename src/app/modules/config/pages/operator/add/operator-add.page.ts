import { Component } from '@angular/core'
import { OperatorService } from '../../../../../services/operator.service'
import { UserSessionHelper } from '../../../../../helpers/user-session.helper'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { Router, ParamMap, ActivatedRoute } from '@angular/router'
import { switchMap } from 'rxjs/operators'
import { CountryService } from 'src/app/services/country.service'
import { MoneyService } from 'src/app/services/money.service';

declare var swal: any

@Component({
    selector: 'app-operator-add-page',
    templateUrl: './operator-add.page.html',
    styleUrls: ['./operator-add.page.scss']
})

export class OperatorAddPage {
    title = 'Operator';
    private frmOperator: FormGroup
    private isUpdate: boolean = false
    private operatorId: number = 0
    private countryList: Array<any> = new Array<any>()
    private moneyList: Array<any> = new Array<any>()

    constructor(
        private operatorService: OperatorService,
        private userSessionHelper: UserSessionHelper,
        private countryService: CountryService,
        private moneyService: MoneyService,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }
    ngOnInit() {
        this.createForm()
        this.listCountry()
        this.listMoney()

        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getOperator(id)
            this.isUpdate = true
            this.operatorId = parseInt(id)
        }
    }

    private listCountry() {
        this.countryService.getAll({ page_size: 100, status: 1 }).subscribe(
            (response) => {
                this.countryList = response.data.results
            },
            (error) => {

            }
        )
    }

    private listMoney() {
        this.moneyService.getAll({ page_size: 20, status: 1 }).subscribe(
            (response) => {
                this.moneyList = response.data.results
            },
            (error) => {

            }
        )
    }

    private getOperator(operatorId) {
        this.operatorService.get(operatorId).subscribe(
            (response) => {
                let data = response.data
                this.frmOperator.patchValue({
                    address: data.address,
                    name: data.name,
                    web: data.web,
                    phone: data.phone,
                    status: data.status,
                    email_one: data.email_one,
                    email_two: data.email_two,
                    email_three: data.email_three,
                    country_id: data.country_id,
                    money_id: data.money_id
                })
            },
            (error) => {

            }
        )
    }

    private createForm() {
        this.frmOperator = this.frmBuilder.group({
            name: new FormControl('', [<any>Validators.required]),
            phone: new FormControl(''),
            web: new FormControl(''),
            address: new FormControl(''),
            email_one: new FormControl(''),
            email_two: new FormControl(''),
            email_three: new FormControl(''),
            country_id: new FormControl(''),
            money_id: new FormControl('0', [<any>Validators.required, <any>Validators.pattern('^[1-9][0-9]*$')]),
            status: 1
        })
    }

    private changeCountry(e) {
        this.frmOperator.controls['country_id'].setValue(e)
    }

    private changeMoney(e) {
        this.frmOperator.controls['money_id'].setValue(e)
    }

    private save() {
        if (this.frmOperator.valid == true) {
            if (this.isUpdate == false) {
                this.create(this.frmOperator.value)
            } else {
                this.update(this.frmOperator.value)
            }
        } else {
            console.log("save", this.frmOperator.get('money_id').errors)
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.operatorService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/operator/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.operatorService.update(data, this.operatorId).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/operator/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}
