import { Component } from '@angular/core'
import { KcoinsService } from '../../../../../services/kcoins.service'
import { Router, ActivatedRoute } from '@angular/router'

import { GlobalVars } from '../../../../../app.settings'
import { MoneyService } from 'src/app/services/money.service'
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms'

declare var swal: any;

@Component({
    selector: 'app-money-add-page',
    templateUrl: './money-add.page.html',
    styleUrls: ['./money-add.page.scss']
})

export class MoneyAddPage {
    private frmMoney: FormGroup
    private isUpdate: boolean = false
    private moneyId: number = 0

    constructor(
        private moneyService: MoneyService,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.createForm()

        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getMoney(id)
            this.isUpdate = true
            this.moneyId = parseInt(id)
        }
    }

    private getMoney(id) {
        this.moneyService.get(id).subscribe(
            (response) => {
                let data = response.data
                this.frmMoney.patchValue({ simbol: data.simbol, name: data.name, status: data.status })
            },
            (error) => {

            })
    }

    private createForm() {
        this.frmMoney = this.frmBuilder.group({
            name: new FormControl('', [<any>Validators.required]),
            simbol: new FormControl(''),
            status: 1
        })
    }

    private save() {
        if (this.frmMoney.valid == true) {
            if (this.isUpdate == false) {
                this.create(this.frmMoney.value)
            } else {
                this.update(this.frmMoney.value)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.moneyService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/money'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.moneyService.update(data, this.moneyId).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/money'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}