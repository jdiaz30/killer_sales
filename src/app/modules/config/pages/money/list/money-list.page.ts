import { Component } from '@angular/core'
import { KcoinsService } from '../../../../../services/kcoins.service'
import { Router } from '@angular/router'

import { GlobalVars } from '../../../../../app.settings'
import { MoneyService } from 'src/app/services/money.service'

declare var swal: any;

@Component({
    selector: 'app-money-list-page',
    templateUrl: './money-list.page.html',
    styleUrls: ['./money-list.page.scss']
})

export class MoneyListPage {

    private moneyList: Array<any> = new Array<any>()

    constructor(
        private moneyService: MoneyService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.listMoneys()
    }

    private listMoneys() {
        this.moneyService.getAll({}).subscribe(
            (response) => {
                this.moneyList = response.data.results
            },
            (error) => {

            })
    }

    private update(moneyId) {
        this.router.navigate(['/config/money/' + moneyId + '/edit']);
    }

    private delete(moneyId) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar esta moneda?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.moneyService.delete(moneyId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                        this.moneyList.map((money, key) => {
                            if (money.id == moneyId) {
                                this.moneyList.splice(key, 1)
                            }
                        })
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

}