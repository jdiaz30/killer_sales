import { Component } from '@angular/core';
import { HotelService } from '../../../../../services/hotel.service';
import { Router } from '@angular/router';

declare var swal: any

@Component({
    selector: 'app-hotel-page',
    templateUrl: './hotel-list.page.html',
    styleUrls: ['./hotel-list.page.scss']
})

export class HotelListPage {

    title = 'Operator';

    private hotelList: Array<any> = new Array<any>()
    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''

    constructor(
        private hotelService: HotelService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list()
    }

    public changePage(page) {
        this.page = page
        this.list()
    }

    public search(value) {
        this.list()
    }

    private list() {
        this.hotelService.getAll({ page_size: this.pageSize, page: this.page, name: this.testSearch }).subscribe(
            (response) => {
                this.hotelList = response.data.results
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private delete(elementId) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este operador?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.hotelService.delete(elementId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")

                        this.hotelList.map((item, key) => {
                            if (item.id == elementId) {
                                this.hotelList.splice(key, 1)
                            }
                        })
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private update(operatorId) {
        this.router.navigate(['/config/hotel/' + operatorId + '/edit']);
    }

    private changeStatus(item, index) {
        let title = (item.status === 1) ? 'Desactivar Hotel' : 'Activar Hotel'
        let text = (item.status === 1) ? ('¿Esta seguro de desactivar al hotel ' + item.name + '?') : ('¿Esta seguro de activar al hotel ' + item.name + '?')
        let status = (item.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.hotelService.update({ status: status }, item.id).subscribe(
                    (response) => {
                        this.hotelList[index] ? this.hotelList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
    }
}
