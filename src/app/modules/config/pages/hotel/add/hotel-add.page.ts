import { Component } from '@angular/core';
import { HotelService } from '../../../../../services/hotel.service';
import { UserSessionHelper } from '../../../../../helpers/user-session.helper';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ParamMap, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';

declare var swal: any

@Component({
    selector: 'app-hotel-add-page',
    templateUrl: './hotel-add.page.html',
    styleUrls: ['./hotel-add.page.scss']
})

export class HotelAddPage {

    title = 'Hotel';

    private frmOperator: FormGroup
    private isUpdate: boolean = false
    private operatorId: number = 0

    constructor(
        private hotelService: HotelService,
        private userSessionHelper: UserSessionHelper,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.createForm()
        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getOperator(id)
            this.isUpdate = true
            this.operatorId = parseInt(id)
        }
    }

    private getOperator(operatorId) {
        this.hotelService.get(operatorId).subscribe(
            (response) => {
                let data = response.data
                this.frmOperator.patchValue({ address: data.address, name: data.name, web: data.web, phone: data.phone, status: data.status })
            },
            (error) => {

            }
        )
    }

    private createForm() {
        this.frmOperator = this.frmBuilder.group({
            name: new FormControl('', [<any>Validators.required]),
            phone: new FormControl(''),
            web: new FormControl(''),
            address: new FormControl(''),
            status: 1
        })
    }

    private cancel() {
        this.router.navigate(['/config/hotel'])
    }

    private save() {
        if (this.frmOperator.valid == true) {
            if (this.isUpdate == false) {
                this.create(this.frmOperator.value)
            } else {
                this.update(this.frmOperator.value)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.hotelService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.hotelService.update(data, this.operatorId).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}
