import { Component } from '@angular/core'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { Router, ParamMap, ActivatedRoute } from '@angular/router'
import { switchMap } from 'rxjs/operators'

import { OperatorService } from '../../../../../services/operator.service'
import { PackageService } from '../../../../../services/package.service'
import { UserSessionHelper } from '../../../../../helpers/user-session.helper'

declare var swal: any

@Component({
    selector: 'app-package-add-page',
    templateUrl: './package-add.page.html',
    styleUrls: ['./package-add.page.scss']
})

export class PackageAddPage {

    title = 'Paquete'
    private operatorsList: Array<any> = new Array<any>()
    private frmPackage: FormGroup
    private isUpdate: boolean = false
    private packageId: number = 0
    private userSession: any

    constructor(
        private packageService: PackageService,
        private operatorService: OperatorService,
        private userSessionHelper: UserSessionHelper,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }
    ngOnInit() {
        this.userSession = this.userSessionHelper.getUser()
        this.createForm()
        this.listOperators()
        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getOperator(id)
            this.isUpdate = true
            this.packageId = parseInt(id)
        }
    }

    private listOperators() {
        let params = { page_size: 50 }

        if (this.userSession.type_user == "operator") {
            params = Object.assign(params, { operator_id: this.userSession.related_id })
            this.frmPackage.patchValue({ operator_id: params['operator_id'] })
        }

        this.operatorService.getAll(params).subscribe(
            (response) => {
                this.operatorsList = response.data.results
            },
            (error) => {

            }
        )
    }

    private getOperator(packageId) {
        this.packageService.get(packageId).subscribe(
            (response) => {
                let data = response.data
                this.frmPackage.patchValue({
                    operator_id: data.operator_id,
                    dscto: data.dscto,
                    nro_act: data.nro_act,
                    status: data.status
                })
            },
            (error) => {

            }
        )
    }

    private createForm() {
        this.frmPackage = this.frmBuilder.group({
            operator_id: new FormControl('', [<any>Validators.required]),
            dscto: new FormControl('', [<any>Validators.required]),
            nro_act: new FormControl('', [<any>Validators.required]),
            status: 1
        })
    }

    private save() {
        if (this.frmPackage.valid == true) {
            if (this.isUpdate == false) {
                this.create(this.frmPackage.value)
            } else {
                this.update(this.frmPackage.value)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.packageService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/package/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.packageService.update(data, this.packageId, {}).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/config/package/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}

