import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { PackageService } from '../../../../../services/package.service'
import { UserSessionHelper } from 'src/app/helpers/user-session.helper';

declare var swal: any

@Component({
    selector: 'app-package-page',
    templateUrl: './package.page.html',
    styleUrls: ['./package.page.scss']
})

export class PackagePage {

    title = 'Paquete'

    private paqueteList: Array<any> = new Array<any>()
    private userSession: any

    constructor(
        private packageService: PackageService,
        private userSessionHelper: UserSessionHelper,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.userSession = this.userSessionHelper.getUser()
        this.listPackages()
    }

    private listPackages() {
        let params = { page_size: 50 }
        if (this.userSession.type_user == "operator") {
            params = Object.assign(params, { operator_id: this.userSession.related_id })
        }
        this.packageService.getAll(params).subscribe(
            (response) => {
                this.paqueteList = response.data.results;
            },
            (error) => {

            }
        )
    }

    private changeStatus(data, index) {
        let title = (data.status === 1) ? 'Desactivar Paquete' : 'Activar Paquete'
        let text = (data.status === 1) ? ('¿Esta seguro de desactivar al paquete?') : ('¿Esta seguro de activar al paquete?')
        let status = (data.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.packageService.update({ status: status }, data.id, { activate: status }).subscribe(
                    (response) => {
                        this.paqueteList[index] ? this.paqueteList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    })
            }
        }).catch(swal.noop)
    }

    private deletePaquete(id, index) {

        let self = this;
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este operador?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                if (value.value == true) {
                    this.packageService.delete(id).subscribe(
                        (response) => {
                            swal("Éxito", response.user_msg, "success")
                            self.paqueteList.splice(index, 1);
                        },
                        (error) => {
                            swal("Error", error.user_msg, "warning")
                        }
                    )
                }
            }
        }).catch(swal.noop)
    }



    private updatePaquete(id) {
        this.router.navigate(['/config/package/' + id + '/edit'])
    }
}
