import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'statusAvatarPipe' })
export class StatusAvatarPipe implements PipeTransform {
    transform(value: number, exponent: string): string {
        let response = ""

        switch (value) {
            case 0:
                response = "Inactivo"
                break
            case 1:
                response = "Disponible"
                break
            case 2:
                response = "No disponible"
                break
        }

        return response
    }
}