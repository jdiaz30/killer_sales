import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'booleanTextPipe' })
export class BooleanTextPipe implements PipeTransform {
    transform(value: boolean, exponent: string): string {
        return (value == true) ? "Si" : "No"
    }
}