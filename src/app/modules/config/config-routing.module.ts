import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'

import { ConfigComponent } from './config.component'
import { OperatorPage } from './pages/operator/list/operator.page'
import { OperatorAddPage } from './pages/operator/add/operator-add.page'
import { CatalogListPage } from './pages/catalog/list/catalog-list.page'
import { CatalogAddPage } from './pages/catalog/add/catalog-add.page'
import { CitiesListPage } from './pages/cities/list/cities-list.page'
import { CitiesAddPage } from './pages/cities/add/cities-add.page'
import { CountryListPage } from './pages/country/list/country-list.page'
import { CountryAddPage } from './pages/country/add/country-add.page'
import { HotelListPage } from './pages/hotel/list/hotel-list.page'
import { HotelAddPage } from './pages/hotel/add/hotel-add.page'
import { UserPage } from './pages/user/list/user.page'
import { UserAddPage } from './pages/user/add/user-add.page'
import { PackagePage } from './pages/package/list/package.page'
import { PackageAddPage } from './pages/package/add/package-add.page'
import { CategoryPage } from './pages/category/list/category.page'
import { CategoryAddPage } from './pages/category/add/category-add.page'
import { KcoinsAddPage } from './pages/kcoins/add/kcoins-add.page'
import { KcoinsListPage } from './pages/kcoins/list/kcoins-list.page'
import { MoneyListPage } from './pages/money/list/money-list.page'
import { MoneyAddPage } from './pages/money/add/money-add.page';
import { DivisaListPage } from './pages/divisa/list/divisa-list.page';
import { DivisaAddPage } from './pages/divisa/add/divisa-add.page';


const routes: Routes = [{
    path: '',
    component: ConfigComponent,
    children: [
        {
            path: 'country',
            component: CountryListPage
        },
        {
            path: 'country/add',
            component: CountryAddPage
        },
        {
            path: 'country/:id/edit',
            component: CountryAddPage
        },
        {
            path: 'cities',
            component: CitiesListPage
        },
        {
            path: 'cities/add',
            component: CitiesAddPage
        },
        {
            path: 'cities/:id/edit',
            component: CitiesAddPage
        },
        {
            path: 'operator',
            component: OperatorPage
        },
        {
            path: 'operator/add',
            component: OperatorAddPage
        },
        {
            path: 'operator/:id/edit',
            component: OperatorAddPage
        },
        {
            path: 'operator/:id/divisas',
            component: DivisaListPage
        },
        {
            path: 'operator/:id/divisas/add',
            component: DivisaAddPage
        },
        {
            path: 'operator/:id/divisas/:divisaId/edit',
            component: DivisaAddPage
        },
        {
            path: 'catalog',
            component: CatalogListPage
        },
        {
            path: 'catalog/add',
            component: CatalogAddPage
        },
        {
            path: 'catalog/:id/edit',
            component: CatalogAddPage
        },
        {
            path: 'user',
            component: UserPage
        },
        {
            path: 'user/add',
            component: UserAddPage
        },
        {
            path: 'user/:id/edit',
            component: UserAddPage
        },
        {
            path: 'hotel',
            component: HotelListPage
        },
        {
            path: 'hotel/add',
            component: HotelAddPage
        },
        {
            path: 'hotel/:id/edit',
            component: HotelAddPage
        },
        {
            path: 'package',
            component: PackagePage
        },
        {
            path: 'package/add',
            component: PackageAddPage
        },
        {
            path: 'package/:id/edit',
            component: PackageAddPage
        },
        {
            path: 'category',
            component: CategoryPage
        },
        {
            path: 'category/add',
            component: CategoryAddPage
        },
        {
            path: 'category/:id/edit',
            component: CategoryAddPage
        },
        {
            path: 'kcoins',
            component: KcoinsListPage
        },
        {
            path: 'kcoins/add',
            component: KcoinsAddPage
        },
        {
            path: 'kcoins/:id/edit',
            component: KcoinsAddPage
        },
        {
            path: 'money',
            component: MoneyListPage
        },
        {
            path: 'money/add',
            component: MoneyAddPage
        },
        {
            path: 'money/:id/edit',
            component: MoneyAddPage
        },
    ],

},

    /* { path: '/config', redirectTo: '/config/operator' }, */
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class ConfigRoutingModule {
}
