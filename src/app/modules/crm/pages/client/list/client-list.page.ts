import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { ClientService } from 'src/app/services/client.service'

declare var swal: any

@Component({
    selector: 'app-crm-client-list-page',
    templateUrl: './client-list.page.html',
    styleUrls: ['./client-list.page.scss']
})

export class ClientListPage {

    title = 'Cliente'
    private clientList: Array<any> = new Array<any>()
    private count = 0
    private pageSize = 30
    private page = 1
    private textSearch: string = ''

    constructor(
        private clientService: ClientService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list()
    }


    private search(value) {
        this.list()
    }

    private list() {
        this.clientService.getAll({ page_size: this.pageSize, page: this.page, name: this.textSearch }).subscribe(
            (response) => {
                this.clientList = response.data.results
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    public changePage(page) {
        this.page = page
        this.list()
    }


}
