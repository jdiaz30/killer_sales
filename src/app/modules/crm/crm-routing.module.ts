import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'

import { CrmComponent } from './crm.component'
import { ClientListPage } from './pages/client/list/client-list.page'

const routes: Routes = [{
    path: '',
    component: CrmComponent,
    children: [
        {
            path: 'client',
            component: ClientListPage
        },
    ],

},

    /* { path: '/config', redirectTo: '/config/operator' }, */
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class CrmRoutingModule {
}
