import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http'

import { ThemeModule } from '../../theme/theme.module'
import { ClientListPage } from './pages/client/list/client-list.page'
import { CrmRoutingModule } from './crm-routing.module'
import { CrmComponent } from './crm.component'

const PAGES_COMPONENTS = [
    CrmComponent,
    ClientListPage,
];

const PIPES = [

]

@NgModule({
    imports: [
        CommonModule,
        CrmRoutingModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule
    ],
    declarations: [
        ...PAGES_COMPONENTS,
        ...PIPES
    ],
    exports: [
        ...PIPES
    ],
    providers: [
    ]
})
export class CrmModule {
}
