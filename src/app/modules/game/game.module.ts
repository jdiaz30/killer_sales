import { NgModule } from '@angular/core';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';


import { ThemeModule } from '../../theme/theme.module';

import { CommonModule } from '../../../../node_modules/@angular/common';

import { PromotorAddPage } from './pages/promotor/add/promotor-add.page';
import { PromotorListPage } from './pages/promotor/list/promotor-list.page';
import { AvatarListPage } from './pages/avatar/list/avatar-list.page';
import { AvatarAddPage } from './pages/avatar/add/avatar-add.page';
import { GameRoutingModule } from './game-routing.module';
import { BooleanTextPipe } from '../config/pipes/BooleanTextPipe';
import { StatusAvatarPipe } from '../config/pipes/StatusAvatarPipe';
import { GameComponent } from './game.component';
import { ConfigModule } from '../config/config.module';
import { ClanListPage } from './pages/clan/list/clan-list.page';

const PAGES_COMPONENTS = [
    PromotorAddPage,
    PromotorListPage,
    AvatarListPage,
    AvatarAddPage,
    ClanListPage,
    GameComponent
];

const PIPES = [
    BooleanTextPipe,
    StatusAvatarPipe
]

@NgModule({
    imports: [
        CommonModule,
        GameRoutingModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        ConfigModule
    ],
    declarations: [
        ...PAGES_COMPONENTS,
    ],
    providers: [
    ]
})
export class GameModule {
}
