import { Component } from '@angular/core';
import { PromotorService } from '../../../../../services/promotor.service';
import { CountryService } from '../../../../../services/country.service';
import { UserSessionHelper } from '../../../../../helpers/user-session.helper';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { OperatorService } from 'src/app/services/operator.service';

declare var swal: any
declare var $: any

@Component({
    selector: 'app-promotor-add-page',
    templateUrl: './promotor-add.page.html',
    styleUrls: ['./promotor-add.page.scss']
})

export class PromotorAddPage {
    title = 'Promotor';
    private frmPromotor: FormGroup
    private isUpdate: boolean = false
    private promotorId: number = 0
    private countrysList: Array<any> = new Array<any>()
    private operatorsList: Array<any> = new Array<any>()

    constructor(
        private promotorService: PromotorService,
        private countryService: CountryService,
        private operatorService: OperatorService,
        private userSessionHelper: UserSessionHelper,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.createForm()
        this.listCountry()
        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getPromotor(id)
            this.isUpdate = true
            this.promotorId = parseInt(id)
        }
    }

    private showOperator() {
        $("#operatorModal").modal('show')
    }

    private searchOperator(event) {
        console.log("searchOperator", event.target.value)

        this.operatorService.getAll({ page_size: 10, page: 1, name: event.target.value }).subscribe(
            (response) => {
                this.operatorsList = response.data.results
            },
            (error) => {

            }
        )
    }

    private handleSelectOperator(operator) {
        console.log("handleSelectOperator", operator)
        this.frmPromotor.patchValue({ operator_id: operator.id, operator_name: operator.name })

        $("#operatorModal").modal('hide')
    }

    private listCountry() {
        this.countryService.getAll({ page_size: 100 }).subscribe(
            (response) => {
                this.countrysList = response.data.results
            },
            (error) => {

            }
        )
    }

    private getPromotor(operatorId) {
        this.promotorService.get(operatorId).subscribe(
            (response) => {
                let data = response.data
                this.frmPromotor.patchValue({
                    address: data.address,
                    name: data.name,
                    email: data.email,
                    phone: data.phone,
                    status: data.status,
                    last_name: data.last_name,
                    country_id: data.country_id,
                    type_promotor: data.type_promotor,
                    operator_id: data.operator_id,
                    operator_name: data.operator !== null ? data.operator.name : ""
                })
            },
            (error) => {

            }
        )
    }

    private createForm() {
        this.frmPromotor = this.frmBuilder.group({
            name: new FormControl('', [<any>Validators.required]),
            last_name: new FormControl('', [<any>Validators.required]),
            phone: new FormControl(''),
            email: new FormControl(''),
            address: new FormControl(''),
            country_id: new FormControl('', [<any>Validators.required]),
            status: 1,
            type_promotor: new FormControl('', [<any>Validators.required]),
            operator_id: "",
            operator_name: ""
        })
    }

    changeCountry(e) {
        this.frmPromotor.controls['country_id'].setValue(e)
    }

    changeTypePromotor(e) {
        this.frmPromotor.controls['type_promotor'].setValue(e)
    }

    private save() {
        if (this.frmPromotor.valid == true) {
            let dataForm = { ... this.frmPromotor.value }

            delete dataForm.operator_name
            if (dataForm.operator_id == "" || dataForm.operator_id == null) {
                delete dataForm.operator_id
            }

            if (this.isUpdate == false) {
                this.create(dataForm)
            } else {
                this.update(dataForm)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.promotorService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/game/promotor/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.promotorService.update(data, this.promotorId, {}).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(['/game/promotor/'])
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}
