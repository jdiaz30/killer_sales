import { Component } from '@angular/core';
import { PromotorService } from '../../../../../services/promotor.service';
import { Router } from '@angular/router';
import { GlobalVars } from '../../../../../app.settings';

declare var swal: any

@Component({
    selector: 'app-promotor-page',
    templateUrl: './promotor-list.page.html',
    styleUrls: ['./promotor-list.page.scss']
})

export class PromotorListPage {
    title = 'Promotors';
    BASE_URL = GlobalVars.API_BASE_URL;

    private promotorsList: Array<any> = new Array<any>()
    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''

    constructor(
        private promotorService: PromotorService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list()
    }

    public changePage(page) {
        this.page = page
        this.list()
    }

    public search(value) {
        this.list()
    }

    private list() {
        this.promotorService.getAll({ page_size: this.pageSize, page: this.page, name: this.testSearch }).subscribe(
            (response) => {
                this.promotorsList = response.data.results
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private deletePromotor(promotorId, index) {
        let self = this
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este promotor?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.promotorService.delete(promotorId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                        self.promotorsList.splice(index, 1);
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private changeStatus(promotor, index) {
        let title = (promotor.status === 1) ? 'Desactivar Promotor' : 'Activar Promotor'
        let text = (promotor.status === 1) ? ('¿Esta seguro de desactivar al promotor ' + promotor.name + '?') : ('¿Esta seguro de activar al promotor ' + promotor.name + '?')
        let status = (promotor.status === 1) ? 0 : 1

        //if (promotor.verify_phone == true || status == 0) {
        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.promotorService.update({ status: status }, promotor.id, { activate: status }).subscribe(
                    (response) => {
                        this.promotorsList[index] ? this.promotorsList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
        // }
        /*  else {
            swal("No se puede habilitar a este Promotor", "Su telefono no ha sido verificado", "info")
        } */
    }

    private updatePromotor(promotorId) {
        this.router.navigate(['/game/promotor/' + promotorId + '/edit']);
    }
}