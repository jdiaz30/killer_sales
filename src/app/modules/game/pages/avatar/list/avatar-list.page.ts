import { Component } from '@angular/core';

import { GlobalVars } from '../../../../../app.settings';
import { AvatarService } from '../../../../../services/avatar.service';
import { Router } from '@angular/router'

declare var swal: any

@Component({
    selector: 'app-avatar-list-page',
    templateUrl: './avatar-list.page.html',
    styleUrls: ['./avatar-list.page.scss']
})

export class AvatarListPage {

    private avatarList: Array<any> = new Array<any>()
    private urlBaseImage = GlobalVars.API_BASE_URL
    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''
    private status: string = ''
    private is_free: string = ''

    constructor(
        private avatarService: AvatarService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list()
    }

    public changePage(page) {
        this.page = page
        this.list()
    }

    public search(value) {
        this.list()
    }

    private list() {
        var filters = { page_size: this.pageSize, page: this.page, name: this.testSearch }
        if (this.status !== '')
            filters['status'] = this.status
        if (this.is_free !== '')
            filters['is_free'] = this.is_free

        this.avatarService.getAll(filters).subscribe(
            (response) => {
                this.avatarList = response.data.results
                this.count = response.data.count
            },
            (error) => {
                console.log("errors", error)
            })
    }

    private updateAvatar(avatarId) {
        this.router.navigate(['/game/avatar/' + avatarId + '/edit']);
    }

    private deleteAvatar(avatarId) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este Avatar?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.avatarService.delete(avatarId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                        this.avatarList.map((avatar, key) => {
                            if (avatar.id == avatarId) {
                                this.avatarList.splice(key, 1)
                            }
                        })
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private changeStatus(item, index) {
        let title = (item.status === 1) ? 'Desactivar Avatar' : 'Activar Avatar'
        let text = (item.status === 1) ? ('¿Esta seguro de desactivar avatar ' + item.name + '?') : ('¿Esta seguro de activar avatar ' + item.name + '?')
        let status = (item.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.avatarService.update({ status: status }, item.id).subscribe(
                    (response) => {
                        this.avatarList[index] ? this.avatarList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
    }

}
