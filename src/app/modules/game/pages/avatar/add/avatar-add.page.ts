import { Component } from '@angular/core';
import { AvatarService } from '../../../../../services/avatar.service';
import { UserSessionHelper } from '../../../../../helpers/user-session.helper';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalVars } from '../../../../../app.settings';

declare var swal: any
declare var $: any

@Component({
    selector: 'app-avatar-add-page',
    templateUrl: './avatar-add.page.html',
    styleUrls: ['./avatar-add.page.scss']
})

export class AvatarAddPage {

    private frmAvatar: FormGroup
    private isUpdate: boolean = false
    private avatar: any = null
    private avatarId: number = 0

    constructor(
        private avatarService: AvatarService,
        private userSessionHelper: UserSessionHelper,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.createForm()

        let id = this.route.snapshot.paramMap.get('id')
        if (id !== null) {
            this.getAvatar(id)

            this.isUpdate = true
            this.avatarId = parseInt(id)
        }
    }

    private getAvatar(avatarId) {
        this.avatarService.get(avatarId).subscribe(
            (response) => {
                let data = response.data
                this.frmAvatar.patchValue({ name: data.name, is_free: data.is_free, status: data.status, kcoins: data.kcoins })
                if (data.poster !== null) {
                    let urlAvatar = GlobalVars.API_BASE_URL + data.url
                    this.setImage(urlAvatar, "url")
                }
            },
            (error) => {

            }
        )
    }

    private createForm() {
        this.frmAvatar = this.frmBuilder.group({
            name: new FormControl('', [<any>Validators.required]),
            is_free: new FormControl(0),
            kcoins: new FormControl(0)
        })
    }

    private onFileChange(event: any) {
        let files = event.target.files
        let data = {
            file: files[0],
            title: "photo "
        }

        this.avatar = files[0]
        this.setImage(files[0], "object")
    }

    private setImage(urlImage, type) {
        let image = (type == "object") ? URL.createObjectURL(urlImage) : urlImage
        $("#avatar").attr("src", image)
    }

    private save() {
        if (this.frmAvatar.valid == true) {

            let data = {
                name: ""
            }
            let formData: FormData = new FormData()
            formData.append("name", this.frmAvatar.value.name)
            formData.append("kcoins", this.frmAvatar.value.kcoins)
            formData.append("status", "1")
            formData.append("is_free", this.frmAvatar.value.is_free)

            if (this.avatar !== null) {
                formData.append("url", this.avatar)
            }

            if (this.isUpdate == false) {
                this.create(formData)
            } else {
                this.update(formData)
            }
        } else {
            swal("Alert", "Ingrese los datos correctamente", "info")
        }
    }

    private create(data) {
        this.avatarService.save(data).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }

    private update(data) {
        this.avatarService.update(data, this.avatarId).subscribe(
            (response) => {
                swal("Éxito", response.user_msg, "success")
            },
            (error) => {
                swal("Error", error.user_msg, "warning")
            })
    }
}
