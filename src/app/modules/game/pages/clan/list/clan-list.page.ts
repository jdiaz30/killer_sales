import { Component } from '@angular/core';
import { ClanService } from '../../../../../services/clan.service';
import { Router } from '@angular/router';
import { GlobalVars } from '../../../../../app.settings';

declare var swal: any

@Component({
    selector: 'app-clan-page',
    templateUrl: './clan-list.page.html',
    styleUrls: ['./clan-list.page.scss']
})

export class ClanListPage {

    title = 'Clanes';
    BASE_URL = GlobalVars.API_BASE_URL;

    private clanesList: Array<any> = new Array<any>()
    private count = 0
    private pageSize = 30
    private page = 1
    private testSearch: string = ''

    constructor(
        private clanService: ClanService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.list()
    }

    public changePage(page) {
        this.page = page
        this.list()
    }

    public search(value) {
        this.list()
    }

    private list() {
        var filters = { page_size: this.pageSize, page: this.page, name: this.testSearch }
        this.clanService.getAll(filters).subscribe(
            (response) => {
                this.clanesList = response.data.results
                this.count = response.data.count
            },
            (error) => {

            }
        )
    }

    private deletePromotor(promotorId) {
        swal({
            title: 'Aviso!',
            text: '¿Esta seguro de eliminar este promotor?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value == true) {
                this.clanService.delete(promotorId).subscribe(
                    (response) => {
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    }
                )
            }

        }).catch(swal.noop)
    }

    private changeStatus(promotor, index) {
        let title = (promotor.status === 1) ? 'Desactivar Clan' : 'Activar Clan'
        let text = (promotor.status === 1) ? ('¿Esta seguro de desactivar al clan ' + promotor.name + '?') : ('¿Esta seguro de activar al clan ' + promotor.name + '?')
        let status = (promotor.status === 1) ? 0 : 1

        swal({
            title: title,
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,lo estoy!'
        }).then((value) => {
            if (value.value === true) {
                this.clanService.update({ status: status }, promotor.id).subscribe(
                    (response) => {
                        this.clanesList[index] ? this.clanesList[index].status = status : null;
                        swal("Éxito", response.user_msg, "success")
                    },
                    (error) => {
                        swal("Error", error.user_msg, "warning")
                    });
            }
        }).catch(swal.noop)
    }

    private updatePromotor(promotorId) {
        this.router.navigate(['/game/clan/' + promotorId + '/edit']);
    }
}