import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { GameComponent } from './game.component';
import { PromotorListPage } from './pages/promotor/list/promotor-list.page';
import { PromotorAddPage } from './pages/promotor/add/promotor-add.page';
import { AvatarListPage } from './pages/avatar/list/avatar-list.page';
import { AvatarAddPage } from './pages/avatar/add/avatar-add.page';
import { ClanListPage } from './pages/clan/list/clan-list.page';



const routes: Routes = [{
    path: '',
    component: GameComponent,
    children: [

        {
            path: 'promotor',
            component: PromotorListPage
        },
        {
            path: 'promotor/add',
            component: PromotorAddPage
        },
        {
            path: 'promotor/:id/edit',
            component: PromotorAddPage
        },
        {
            path: 'avatar',
            component: AvatarListPage
        },
        {
            path: 'avatar/add',
            component: AvatarAddPage
        },
        {
            path: 'avatar/:id/edit',
            component: AvatarAddPage
        },
        {
            path: 'clan',
            component: ClanListPage
        },
    ],

},

    /* { path: '/config', redirectTo: '/config/operator' }, */
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class GameRoutingModule {
}
