import { NgModule } from '@angular/core';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';


import { ThemeModule } from '../../theme/theme.module';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { MainPage } from './pages/main/main.page';


const PAGES_COMPONENTS = [
    MainPage,
    DashboardComponent
];

@NgModule({
    imports: [
        DashboardRoutingModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule
    ],
    declarations: [
        ...PAGES_COMPONENTS,
    ],
    providers: [
    ]
})
export class DashboardModule {
}
