import { Component } from '@angular/core';

declare var $: any
@Component({
    selector: 'module-dashboard',
    template: '<theme-layout-main></theme-layout-main>'
})
export class DashboardComponent {

    constructor() {

    }

    ngOnInit() {
        $('body').addClass("vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar")
    }
}
