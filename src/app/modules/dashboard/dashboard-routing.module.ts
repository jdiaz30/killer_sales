import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard.component';
import { MainPage } from './pages/main/main.page';

const routes: Routes = [{
    path: '',
    component: DashboardComponent,
    children: [
        {
            path: '',
            component: MainPage
        }],

}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class DashboardRoutingModule {
}
