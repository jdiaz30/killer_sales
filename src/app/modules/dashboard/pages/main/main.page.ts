import { Component } from '@angular/core'
import { DashboardService } from 'src/app/services/dashboard.service'
import * as moment from 'moment'
import { UserSessionHelper } from 'src/app/helpers/user-session.helper';
import { delay } from 'rxjs/operators';

declare const Chartist

@Component({
    selector: 'app-main-page',
    templateUrl: './main.page.html',
    styleUrls: ['./main.page.scss']
})

export class MainPage {

    private userSession = null
    private stats = {}

    constructor(
        private dashboardService: DashboardService,
        private userSessionHelper: UserSessionHelper
    ) {

    }

    ngOnInit() {
        this.userSession = this.userSessionHelper.getUser()
        this.listStats()
    }

    private listStats() {
        let data = {
            date_order: moment.utc().format("YYYY-MM-DD")
        }

        this.dashboardService.getStats(data).subscribe(
            (response) => {
                let deals = { ...response.data.deals }
                deals['pendients'] = (deals['pendients'] == 0) ? 0.1 : deals['pendients']
                deals['reservations'] = (deals['reservations'] == 0) ? 0.1 : deals['reservations']
                deals['payments'] = (deals['payments'] == 0) ? 0.1 : data['payments']
                deals['canceleds'] = (deals['canceleds'] == 0) ? 0.1 : deals['canceleds']

                this.stats = response.data

                this.lineChart(deals)
            },
            (error) => {

            })
    }

    private lineChart(data: any) {
        var verticalBar = new Chartist.Bar('#vertical-bar', {
            labels: ['Pendientes', 'Reservados', 'Pagados', 'Cancelados'],
            series: [
                [data['pendients'], data['reservations'], data['payments'], data['canceleds']]
            ]
        }, {
                axisY: {
                    labelInterpolationFnc: function (value) {
                        return (value) + 'k';
                    },
                    scaleMinSpace: 50,
                },
                axisX: {
                    showGrid: false
                },
                plugins: [
                    Chartist.plugins.tooltip({
                        appendToBody: true
                    })
                ]
            });
        verticalBar.on('draw', function (data) {
            if (data.type === 'bar') {
                data.element.attr({
                    style: 'stroke-width: 30px',
                    y1: 350,
                    x1: data.x1 + 0.001
                });
                data.group.append(new Chartist.Svg('circle', {
                    cx: data.x2,
                    cy: data.y2,
                    r: 15
                }, 'ct-slice-pie'));
            }
        });
        verticalBar.on('created', function (data) {
            var defs = data.svg.querySelector('defs') || data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'barGradient2',
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(253,99,107,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(253,99,107, 0.6)'
            });
            return defs;
        });
    }



    private pieChartNewKillers() {
        var data = {
            series: [36, 64]
        };
        var sum = function (a, b) {
            return a + b;
        };
        new Chartist.Pie('#pie-chart', data, {
            labelInterpolationFnc: function (value) {
                return Math.round(value / data.series.reduce(sum) * 100) + '%';
            }
        });
    }
}
