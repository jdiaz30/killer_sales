import { Component } from '@angular/core';
declare var $: any

@Component({
    selector: 'app-not-found',
    templateUrl: './not-found.component.html',
    styleUrls: ['./not-found.component.scss']
})

export class NotFoundComponent {

    ngOnInit() {
        $('body').removeClass("vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar");
        //.addClass("vertical-layout vertical-menu 1-column  bg-gradient-directional-danger menu-expanded blank-page blank-page  pace-done");

    }
}