import { NgModule } from '@angular/core'

import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http'
import { NgxPermissionsModule } from 'ngx-permissions'

import { AuthComponent } from './auth.component'
import { LoginPage } from './pages/login/login.page'

import { AuthRoutingModule } from './auth-routing.module'
import { ThemeModule } from '../../theme/theme.module'


const PAGES_COMPONENTS = [
    AuthComponent,
    LoginPage
];

@NgModule({
    imports: [
        AuthRoutingModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        NgxPermissionsModule.forChild()
    ],
    declarations: [
        ...PAGES_COMPONENTS,
    ],
    providers: [
    ]
})
export class AuthModule {
}
