import { Component } from '@angular/core'
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms'
import { NgxPermissionsService } from 'ngx-permissions'

import { AuthService } from '../../../../services/auth.service'
import { Router } from '../../../../../../node_modules/@angular/router'
import { UserSessionHelper } from '../../../../helpers/user-session.helper'
import { GlobalHelper } from 'src/app/helpers/global.helper'


declare var $: any
declare var swal: any

@Component({
    selector: 'app-login-page',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss']
})
export class LoginPage {
    title = 'Login';
    private frmLogin: FormGroup

    constructor(
        private authService: AuthService,
        private userSessionHelper: UserSessionHelper,
        private globalHelper: GlobalHelper,
        private frmBuilder: FormBuilder,
        private permissionsService: NgxPermissionsService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.actionByAuthentication()
        $('body').removeClass("vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar")
        this.createFormLogin()
    }

    private createFormLogin() {
        this.frmLogin = this.frmBuilder.group({
            user_name: new FormControl('', [<any>Validators.required]),
            password: new FormControl('', [<any>Validators.required, <any>Validators.maxLength(19)])
        })
    }

    private actionByAuthentication() {
        let session = this.userSessionHelper.getToken()
        if (session !== null) {
            this.router.navigate(['dashboard'])
        }
    }

    private login() {
        this.globalHelper.showLoading()
        this.authService.auth(this.frmLogin.value, { source: "admin" }).subscribe(
            (response) => {
                this.globalHelper.hideShowLoading(window, 500)
                this.userSessionHelper.setToken(response.data.token)

                let user = this.userSessionHelper.getUser()
                const perm = [user.type_user]
                this.permissionsService.loadPermissions(perm)
                swal("Éxito", response.user_msg, "success")
                this.router.navigate(["dashboard"])
            },
            (error) => {
                this.globalHelper.hideShowLoading(window, 500)
                swal("Error", error.user_msg, "warning")
            }
        )
    }
}
