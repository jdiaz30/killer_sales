import { Component } from '@angular/core';

@Component({
    selector: 'module-auth',
    //template: '<router-outlet></router-outlet>',
    template: '<theme-layout-auth></theme-layout-auth>'
})
export class AuthComponent {
}
