import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthComponent } from './auth.component';
import { LoginPage } from './pages/login/login.page';


const routes: Routes = [{
    path: '',
    component: AuthComponent,
    children: [
        {
            path: '',
            component: LoginPage
        }],

}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AuthRoutingModule {
}
