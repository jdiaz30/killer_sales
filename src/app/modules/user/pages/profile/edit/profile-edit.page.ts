import { Component } from '@angular/core';
import { AvatarService } from '../../../../../services/avatar.service';
import { UserSessionHelper } from '../../../../../helpers/user-session.helper';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalVars } from '../../../../../app.settings';

declare var swal: any
declare var $: any

@Component({
    selector: 'app-profile-edit-page',
    templateUrl: './profile-edit.page.html',
    styleUrls: ['./profile-edit.page.scss']
})

export class ProfileEditPage {

    constructor(
        private avatarService: AvatarService,
        private userSessionHelper: UserSessionHelper,
        private frmBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {

    }

}
