import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { UserComponent } from './user.component';
import { ProfileEditPage } from './pages/profile/edit/profile-edit.page';



const routes: Routes = [{
    path: '',
    component: UserComponent,
    children: [
        {
            path: 'profile',
            component: ProfileEditPage
        }
    ],

},

    /* { path: '/config', redirectTo: '/config/operator' }, */
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class UserRoutingModule {
}
