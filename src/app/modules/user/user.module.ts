import { NgModule } from '@angular/core'

import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http'
import { ThemeModule } from '../../theme/theme.module'

import { CommonModule } from '../../../../node_modules/@angular/common'

import { ProfileEditPage } from './pages/profile/edit/profile-edit.page'
import { UserRoutingModule } from './user-routing.module'
import { BooleanTextPipe } from '../config/pipes/BooleanTextPipe'
import { StatusAvatarPipe } from '../config/pipes/StatusAvatarPipe'
import { UserComponent } from './user.component'
import { ConfigModule } from '../config/config.module'

const PAGES_COMPONENTS = [
    ProfileEditPage,
    UserComponent
]

const PIPES = [
    BooleanTextPipe,
    StatusAvatarPipe
]

@NgModule({
    imports: [
        CommonModule,
        UserRoutingModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        ConfigModule,

    ],
    declarations: [
        ...PAGES_COMPONENTS,
    ],
    providers: [
    ]
})

export class UserModule {
}
