import { Component } from '@angular/core'

declare var $: any
@Component({
    selector: 'user-profile',
    //template: '<router-outlet></router-outlet>',
    template: '<theme-layout-main></theme-layout-main>'
})
export class UserComponent {

    constructor() {

    }

    ngOnInit() {
        $('body').addClass("vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar")
    }
}
