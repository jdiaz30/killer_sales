import { Injectable } from '@angular/core';
import { Headers, Http, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import { map, take, timeout, catchError } from 'rxjs/operators';

import { GlobalVars } from '../app.settings';
import { HttpHelper } from '../helpers/http.helper';
import { HttpService } from './http.service';

@Injectable()
export class OrderService {

    private headers = new Headers({
        'Content-Type': 'application/octet-stream',
        'Accept': 'application/octet-stream'
    })


    constructor(private http: Http, private httpHelper: HttpHelper, private httpAuth: HttpService) {
    }

    public getAll(params: any): Observable<any> {
        let paramsParse = this.httpHelper.parseParamsUrlGet(params)
        return this.httpAuth.get(GlobalVars.API_URL + "/orders/", { search: params })
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public get(orderId: number): Observable<any> {
        return this.httpAuth.get(GlobalVars.API_URL + "/orders/" + orderId + "/")
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public getStats(params: any): Observable<any> {
        let paramsParse = this.httpHelper.parseParamsUrlGet(params)
        return this.httpAuth.get(GlobalVars.API_URL + "/orders/stats/", { search: params })
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public updateStatus(orderId: number, data: any, params: any): Observable<any> {
        let paramsParse = this.httpHelper.parseParamsUrlGet(params)
        return this.httpAuth.post(GlobalVars.API_URL + "/orders/" + orderId + "/status/", data, { search: params })
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public updateOrderActivity(params: any, orderId: number, data: any): Observable<any> {
        let paramsParse = this.httpHelper.parseParamsUrlGet(params)
        return this.httpAuth.put(GlobalVars.API_URL + "/orders/" + orderId + "/activities/", data, { search: params })
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public getAllToExcel(params: any): Observable<any> {
        let paramsParse = this.httpHelper.parseParamsUrlGet(params)
        return this.httpAuth.get(GlobalVars.API_URL + "/orders/report/excel/", {
            search: params, headers: this.headers
        })
            //.pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

}