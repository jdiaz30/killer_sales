import { Injectable } from '@angular/core'
import { Headers, Http, URLSearchParams, RequestOptions } from '@angular/http'
import { Observable } from 'rxjs'

import { map, take, timeout, catchError } from 'rxjs/operators'

import { GlobalVars } from '../app.settings'
import { HttpHelper } from '../helpers/http.helper'
import { HttpService } from './http.service'

@Injectable()
export class MoneyService {

    private headers = new Headers({ 'Content-Type': 'application/json' })

    constructor(private http: Http, private httpHelper: HttpHelper, private httpAuth: HttpService) {
    }

    public get(id): Observable<any> {
        return this.httpAuth.get(GlobalVars.API_URL + "/moneys/" + id + "/")
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public getAll(params: any): Observable<any> {
        let paramsParse = this.httpHelper.parseParamsUrlGet(params)
        return this.httpAuth.get(GlobalVars.API_URL + "/moneys/", { search: params })
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public delete(id: number): Observable<any> {
        return this.httpAuth.delete(GlobalVars.API_URL + "/moneys/" + id + "/")
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public save(data: any): Observable<any> {
        return this.httpAuth.post(GlobalVars.API_URL + "/moneys/", data)
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public update(data: any, id: number): Observable<any> {
        return this.httpAuth.put(GlobalVars.API_URL + "/moneys/" + id + "/", data)
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

}