import { Injectable } from '@angular/core';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs';


import { GlobalVars } from '../app.settings';

import { UserSessionHelper } from '../helpers/user-session.helper';

@Injectable()
export class HttpService extends Http {
    private userSessionHelper: UserSessionHelper = new UserSessionHelper();

    constructor(
        backend: XHRBackend,
        options: RequestOptions,
    ) {
        //this.options.headers.set('Content-Type','application/json');
        super(backend, options);
    }
    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        let token = this.userSessionHelper.getToken()
        let self = this;

        if (typeof url === 'string') { // meaning we have to add the token to the options, not in url
            if (!options) {
                // let's make option object
                options = { headers: new Headers() };
            }
            options.headers.set('Authorization', `JWT ${token || ''}`);

        } else {
            // we have to add the token to the url object
            url.headers.set('Authorization', `JWT ${token || ''}`);
        }

        return super.request(url, options);
    }
}