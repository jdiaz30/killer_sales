import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { UserSessionHelper } from '../helpers/user-session.helper';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(
        private userSessionHelp: UserSessionHelper,
        private router: Router
    ) {

    }

    canActivate() {
        let session = this.userSessionHelp.getToken();
        console.log("canActivate", session);
        if (session == null) {
            this.router.navigate(['auth']);
        }

        return session == null ? false : true;
    }
}