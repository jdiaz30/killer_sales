import { Injectable } from '@angular/core';
import { Headers, Http, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import { map, take, timeout, catchError } from 'rxjs/operators';

import { GlobalVars } from '../app.settings';
import { HttpHelper } from '../helpers/http.helper';

@Injectable()
export class AuthService {

    private headers = new Headers({ 'Content-Type': 'application/json' });


    constructor(private http: Http, private httpHelper: HttpHelper) {
    }
    //Auth api
    public auth(data, params): Observable<any> {
        return this.http.post(GlobalVars.API_URL + "/auth/", data, { search: params })
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))

    }
}