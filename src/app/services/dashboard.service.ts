import { Injectable } from '@angular/core';
import { Headers, Http, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import { map, take, timeout, catchError } from 'rxjs/operators';

import { GlobalVars } from '../app.settings';
import { HttpHelper } from '../helpers/http.helper';
import { HttpService } from './http.service';

@Injectable()
export class DashboardService {

    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http, private httpHelper: HttpHelper, private httpAuth: HttpService) {
    }

    public getStats(params: any): Observable<any> {
        let paramsParse = this.httpHelper.parseParamsUrlGet(params)
        return this.httpAuth.get(GlobalVars.API_URL + "/dashboard/", { search: paramsParse })
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }
}