import { Injectable } from '@angular/core';
import { Headers, Http, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import { map, take, timeout, catchError } from 'rxjs/operators';

import { GlobalVars } from '../app.settings';
import { HttpHelper } from '../helpers/http.helper';
import { HttpService } from './http.service';

@Injectable()
export class ActivityService {

    private headers = new Headers({ 'Content-Type': 'application/json' });


    constructor(private http: Http, private httpHelper: HttpHelper, private httpAuth: HttpService) {
    }


    public getAll(params: any): Observable<any> {
        let paramsParse = this.httpHelper.parseParamsUrlGet(params)

        return this.httpAuth.get(GlobalVars.API_URL + "/activities/", { search: params })
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))

    }

    public get(id: any): Observable<any> {
        return this.httpAuth.get(GlobalVars.API_URL + "/activities/" + id + "/", {})
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public save(data: any, params: any): Observable<any> {
        let paramsParse = this.httpHelper.parseParamsUrlGet(params)
        return this.httpAuth.post(GlobalVars.API_URL + "/activities/", data, { search: paramsParse })
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public update(data: any, activityId: number): Observable<any> {
        return this.httpAuth.put(GlobalVars.API_URL + "/activities/" + activityId + "/", data)
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public clone(data: any): Observable<any> {
        return this.httpAuth.post(GlobalVars.API_URL + "/activities/clone/", data)
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public getPickups(activityId: number, params: any): Observable<any> {
        let paramsParse = this.httpHelper.parseParamsUrlGet(params)
        return this.httpAuth.get(GlobalVars.API_URL + "/activities/" + activityId + "/pickups/", { search: paramsParse })
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

    public clonePickups(activityId: number, data: any): Observable<any> {
        return this.httpAuth.post(GlobalVars.API_URL + "/activities/" + activityId + "/pickups/clone/", data)
            .pipe(map(response => response.json()))
            .pipe(catchError(this.httpHelper.handleError))
    }

}