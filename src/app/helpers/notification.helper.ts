import { Injectable } from '@angular/core';
//import { JwtHelper } from 'angular2-jwt';
import { JwtHelperService } from '@auth0/angular-jwt';

declare var $: any

@Injectable()
export class NotificationHelper {

    constructor() {

    }

    public init(callback: Function) {
        Notification.requestPermission().then(function (permission) {
            if (permission === "granted") {
                callback()
            }
        })
    }

    public showPrevious(title, message, action) {
        if ((Notification as any).permission === "granted") {
            this.show(title, message, action)
            //console.log("entre aqui", (Notification as any).permission)
        } else if ((Notification as any).permission !== 'denied') {
            this.init(() => {
                this.show(title, message, action)
            })
        }
    }

    private show(title, message, action) {
        let options = {
            body: message,
            icon: "./assets/img/icon-40.png",
        }

        let n = new Notification(title, options)
        $("#sound-notify")[0].play()

        n.onclick = (event) => {
            event.preventDefault()
            console.log("Notification click")
        }
    }
}