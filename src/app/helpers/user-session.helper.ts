import { Injectable } from '@angular/core';
//import { JwtHelper } from 'angular2-jwt';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable()

export class UserSessionHelper {
    private jwtHelper: JwtHelperService

    constructor(
    ) {
        this.jwtHelper = new JwtHelperService()
    }

    public setToken(token: any) {
        return localStorage.setItem("killersales_user_session", token)
    }

    public getToken(): any {
        return localStorage.getItem("killersales_user_session") || null
    }

    public getUser(): any {
        let userSession = this.getToken()
        userSession = (this.getToken() == null) ? "" : this.jwtHelper.decodeToken(userSession)
        return userSession
    }

    //Eliminar los datos de session
    public clearSession() {
        localStorage.clear()
    }
}