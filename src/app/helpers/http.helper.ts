
import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';

import { of } from '../../../node_modules/rxjs';
import { Observable } from 'rxjs';


@Injectable()
export class HttpHelper {
    constructor() {
    }

    public handleError(error: Response | any): Promise<any> {
        let errMsg: any;

        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            //errMsg = `${error.status} - ${error.statusText || ''} - ${err.user_msg}`;
            errMsg = {
                status: error.status,
                status_text: error.statusText || '',
                user_msg: body.user_msg || ''
            }

        } else {
            //errMsg = error.message ? error.message : error.toString();
            errMsg = {
                user_msg: error.message ? error.message : error.toString()
            }

        }

        return Promise.reject(errMsg);
    }

    public handleErrorNew<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    public parseParamsUrlGet(data: any): URLSearchParams {
        let params = new URLSearchParams();
        for (let key in data) {
            params.set(key, data[key]);
        }
        return params;
    }
}