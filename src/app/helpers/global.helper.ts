import { Injectable } from '@angular/core'
import { JwtHelperService } from '@auth0/angular-jwt'
import { UserSessionHelper } from './user-session.helper'

declare var $: any
@Injectable()
export class GlobalHelper {

    constructor(
        private userSessionHelper: UserSessionHelper
    ) {

    }

    public showLoading(): any {
        let block = $.blockUI({
            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            timeout: 0, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#FFF',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        })

        return block
    }

    public hideShowLoading(id, timeout) {
        setTimeout(() => {
            $(id).unblock()
        }, timeout)
    }

    public setOperatorParams(params: any) {
        let userSession = this.userSessionHelper.getUser()
        if (userSession.type_user == "operator") {
            params = Object.assign(params, { operator_id: userSession.related_id })
        }
        return params
    }
}