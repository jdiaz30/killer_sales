
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'

import { UserSessionHelper } from './user-session.helper'
import { GlobalVars } from '../app.settings'

declare var io: any

@Injectable()
export class SocketHelper {
    private socket: any;
    private subscribersCounter: number = 0
    constructor(
        private userSessionHelper: UserSessionHelper,
    ) {
    }

    public init() {

        let self = this

        this.socket = io(GlobalVars.API_NODE_URL, { secure: true, reconnect: true })

        this.socket.on('connect', function () {
            console.log("conectado")
        })

        this.socket.on('error', function (err) {
            throw new Error(err)
        })

        this.socket.on('disconnect', function () {
            console.log("desconectado")
        });

        this.socket.on('reconnect_attempt', () => {
            console.log("reconectando")
            // self.createRoom(userSession.user_id)
        });

    }

    public getSocketInstance(): any {
        return this.socket
    }

    public createRoom(userId: number) {
        this.socket.emit("killer_sales:create-room", userId)
    }

    /** create an Observable from an event */
    public fromEvent<T>(eventName: string): Observable<T> {
        this.subscribersCounter++
        return Observable.create((observer: any) => {
            this.socket.on(eventName, (data: T) => {
                observer.next(data)
            });
            return () => {
                if (this.subscribersCounter === 1)
                    this.socket.removeListener(eventName)
            }
        })
    }

    public removeListener(eventName: string, callback?: Function) {
        return this.socket.removeListener.apply(this.socket, arguments)
    }
}